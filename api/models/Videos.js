const mongoose = require("mongoose");

const VideoSchema = new mongoose.Schema({
    title: {
        type: String,
        required: [true, "Please provide title"],
        maxLength: 250,
        minLength: 8
    },
    description: {
        type: String,
        required: [true, "Please provide email"]
    },
    tags: String,
    category: String,
    video: String,
    thumbnail: String,
    wallpaper: String,
    hours: Number,
    minutes: Number,
    seconds: Number,
    createdAt: Number,
    watch: Number,
    views: {
        type: Number,
        default: 0
    },
    album: String,
    likers: Array,
    dislikers: Array,
    comments: [{
        _id: mongoose.Types.ObjectId,
        user: {
            _id: mongoose.Types.ObjectId,
            name: String,
            image: String,
        },
        comment: String,
        createdAt: Date,
        replies: Array
    }],
    user: {
        _id: mongoose.Types.ObjectId,
        name: String,
        image: String,
        subscribers: {
            type: Number,
            default: 0
        }
    },
    year: String,
    isFeatured: {
        type: Boolean,
        default: false
    }
}, {timestamps: true});

module.exports = mongoose.model("Video", VideoSchema);