const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const UserSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: [true, "Please provide your first name."],
        maxLength: 250,
        minLength: 2
    },
    lastName: {
        type: String,
        required: [true, "Please provide your last name."],
        maxLength: 250,
        minLength: 2
    },
    email: {
        type: String,
        required: [true, "Please provide email"],
        unique: true
    },
    password: {
        type: String,
        required: [true, "Please provide password"],
        minLength: 6
    },
    coverPhoto: {
        type: String,
        default: ""
    },
    image: {
        type: String,
        default: ""
    },
    subscribers: {
        type: Number,
        default: 0
    },
    history: Array,
    subscriptions: Array,
    playlists: Array,
    videos: Array,
    notifications: [{
        _id: mongoose.Types.ObjectId,
        notif_type: {
            type: String,
            default: "new comment"
        },
        content: String,
        is_read: {
            type: Boolean,
            default: false
        },
        video_watch: Number,
        user: {
            _id: mongoose.Types.ObjectId,
            name: String,
            image: String
        }
    }],
    isAdmin: {
        type: Boolean,
        default: false
    }
}, {timestamps: true});

UserSchema.pre("save", async function() {
    const salt = await bcrypt.genSalt(10);
    this.password = await bcrypt.hash(this.password, salt);
});


UserSchema.methods.comparePassword = async function(userPassword) {
    const isMatch = await bcrypt.compare(userPassword, this.password);
    return isMatch;
}

UserSchema.methods.createJWT = function () {
    return jwt.sign(
        {userId: this._id, isAdmin: this.isAdmin},
        process.env.SECRET_KEY,
        {expiresIn: "30m"}
    );
}

UserSchema.methods.refreshJWT = function () {
    return jwt.sign(
        {userId: this._id, isAdmin: this.isAdmin},
        process.env.REFRESH_KEY,
        {expiresIn: "1h"}  
    );
}

module.exports = mongoose.model("User", UserSchema);