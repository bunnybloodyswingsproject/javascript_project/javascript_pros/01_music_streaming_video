require("dotenv").config();
const express = require("express");
const app = express();
const connectDB = require("./db/connection");
const authRoutes = require("./routes/auth");
const bodyParser = require("body-parser");
const userRoutes = require("./routes/user");
const cookieParser = require("cookie-parser");
const videoRoutes = require("./routes/video");
const cors = require("cors");
// const userRoutes = require("./routes/auth");
// const musicRoutes = require("./routes/music");

// Request Parser
app.use(express.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());
app.use(cookieParser());
const corsOptions ={
    origin:'http://localhost:3000', 
    credentials:true,            //access-control-allow-credentials:true
    optionSuccessStatus:200
}
app.use(cors(corsOptions));


// Routes
app.use("/api/auth", authRoutes);
app.use("/api/user", userRoutes);
app.use("/api/videos", videoRoutes);

app.listen(8000, () => {
    connectDB(process.env.MONGO_URI);
    console.log("Backend is running");
})

