const User = require("../models/User");
const register = async (req, res) => {
    try {
        const newUser = await User.create({...req.body});
        res.send({
            success: true,
            newUser,
            message: "Successfully added a user."
        })
    } catch(err) {
        if(err.code === 11000) {
            return res.send({
                success: false,
                message: "Email already exists"
            })
        }

        return {
            success: false,
            message: "Somethings wrong with register"
        }
        
    }
}

const login = async (req, res) => {
    try {
        const user = await User.findOne({
            email: req.body.email
        });

        if(!user) {
            return res.send({
                success: false,
                message: "Email or password is not correct."
            })
        }

        const isPasswordCorrect = await user.comparePassword(req.body.password);
        if(!isPasswordCorrect) {
            return res.send({
                success: false,
                message: "Password is incorrect"
            });
        }

        let token = user.createJWT();
        let refreshToken = user.refreshJWT();

        const {password, ...info} = user._doc;
        res.cookie("jwt", refreshToken, {httpOnly: true, maxAge: 24 * 60 * 60 * 1000});
        res.status(200).json({...info, token});
        
    } catch(err) {
        console.log("login error", err);
        res.send({
            status: false,
            message: "error with user login"
        })
    }
}

module.exports = {
    register,
    login
}