const User = require("../models/User");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const { default: mongoose } = require("mongoose");

const generateSalt = async (password) => {
    const salt = await bcrypt.genSalt(10);
    let newPassword = await bcrypt.hash(password, salt);
    return newPassword;
}

const refresh = async (req, res) => {
    const cookies = req.cookies;
    if(!cookies?.jwt) return res.status(401).json({"message": "Cannot find the users token"});
    const refreshToken = cookies.jwt;
    const foundUser = User.find({token: refreshToken});
    if(!foundUser) return res.status(403).json({"message": "Cannot find the refreshed users"});
    jwt.verify(
        refreshToken,
        process.env.REFRESH_KEY,
        (err, decoded) => {
            if(err || foundUser._id !== decoded._id) return res.status(403).json({"message": "Cannot decode user for refreshing token"});
            const token = jwt.sign(
                {"_id": decoded._id},
                process.env.SECRET_KEY,
                {expiresIn: "1h"}
            );

            res.json({token})
        }
    )
}

const findUsers = async (req, res) => {
    try {
        const users = await User.find({}).exec();
        if(!users) {
            return res.send({
                status: true,
                message: "no existing users"
            });
        }

        res.status(200).json(users);
    }catch(error) {
        console.log("findUser error", error);
        res.send({
            status: false,
            message: "error when fetching users"
        })
    }
}

const persistAuth = async (req, res) => {
    const cookies = req.cookies;
    if(!cookies?.jwt) return res.status(401).json({"message": "Cannot find users token"});
    const refreshToken = cookies.jwt;
    jwt.verify(
        refreshToken,
        process.env.REFRESH_KEY,
        async (err, decoded) => {
            if(err) return res.status(401).send({success: false, "message": "Cannot decode the persisting token"});
            const user = await User.findById({
                _id: decoded.userId
            });
            res.send({
                success: true,
                user
            })
        }
    )

}

const avatarUpdate = async (req,res) => {
    const userID = req.body._id;
    const avatarImage = req.body.image;

    try {
        if(!userID || !avatarImage) return res.status(400).send({success: false, message: "Required user authentication and avatar image to process"});
        const user = await User.findOneAndUpdate({
            _id: userID
        }, {
            $set: {
                "image": avatarImage
            }
        });

        if(!user) return res.status(401).send({success: false, message: "Cannot find Update avatar with provided user ID"}); 

        res
            .status(201)
            .send({
            success: true,
            "message": "Successfully updated the user image"
        });
    }catch(error) {
        console.log(error);
        return res.send({
            success: false,
            message: "Something wrong with updating user avatar"
        });
    }
    
}

const getUsers = async(req, res) => {
    try {
        const users = await User.find({}).exec();
        return res.send({
            success: true,
            message: "Successfully fetched all the users",
            user: users
        });
    }catch(error) {
        console.log(error);
        return res.send({
            success: false,
            message: "There's something wrong with getting all users"
        });
    }
}

const updateUser = async(req, res) => {
    try {
        const { password, userId, coverPhoto } = req.body;
        let newPassword = await generateSalt(password).then(data => {
            return data
        });
        await User.updateOne({
            _id: mongoose.Types.ObjectId(userId)
        }, {
            $set: {
                password: newPassword,
                coverPhoto: coverPhoto
            }
        });

        return res.send({
            success: true,
            message: "You successfully updated your profile."
        });
    }catch(error) {
        console.log(error);
        return res.send({
            success: true,
            message: "There's something wrong with user management updates."
        })
    }
}

module.exports = {
    refresh,
    findUsers,
    persistAuth,
    avatarUpdate,
    getUsers,
    updateUser
}