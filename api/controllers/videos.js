const { getVideoDurationInSeconds } = require("get-video-duration");
const User = require("../models/User");
const Video = require("../models/Videos");
const mongoose = require("mongoose");
const { findOneAndDelete } = require("../models/User");

const getUser = (id, callback) => {
    try {
        const user = User.findOne({_id: id}, function (error, result) {
            callback(result);
        });

        if(!user) {
            return {
                "success": false,
                "message": "Invalid user Id in getuser functions"
            }
        }
    }catch(error) {
        console.log(error);
        return {
            "success": false,
            "message": "Something wrong with fetching the users data"
        }
    }
}

const createVideo = async (req, res) => {
    try {
        let title = req.body.title;
        let description = req.body.description;
        let tags = req.body.tags;
        let category = req.body.category;
        let imported_video = req.body.video;
        let thumbnail = req.body.thumbnail;
        let wallpaper = req.body.wallpaper;
        const userID = req.body.userId;
        let year = req.body.year;
        let isFeatured =  req.body.isFeatured;

        await Video.updateMany({}, {
            $set: {
                "isFeatured": false
            }
        });
    
        getUser(userID, function (result) {        
            let currentTime = new Date().getTime();
            getVideoDurationInSeconds(imported_video)
                .then(async function (duration) {
                    let hours = Math.floor(duration/60/60);
                    let minutes = Math.floor(duration/60) - (hours * 60);
                    let seconds = Math.floor(duration % 60);
                    
                    await Video.create({
                        "user": {
                            "_id": result._id,
                            "name": `${result.firstName} ${result.lastName}`,
                            "image": result.image,
                            "subcribers": result.subscribers
                        },
                        "video": imported_video,
                        "thumbnail": thumbnail,
                        "wallpaper": wallpaper,
                        "title": title,
                        "description": description,
                        "tags": tags,
                        "category": category,
                        "createdAt": currentTime,
                        "minutes": minutes,
                        "seconds": seconds,
                        "hours": hours,
                        "watch": currentTime,
                        "views": 0,
                        "album": "",
                        "likers": [],
                        "dislikers": [],
                        "year": year,
                        "isFeatured": isFeatured
                    }, async function(error, data) {
                        await User.updateOne({
                            _id: userID,
                        }, {
                            $push: {
                                "videos": {
                                    "_id": data._id,
                                    "title": title,
                                    "views": 0,
                                    "thumbnail": thumbnail,
                                    "watch": currentTime
                                }
                            }
                        });
    
                        res.status(201)
                           .send({
                            "success": true,
                            "message": "Successfully uploaded a file"
                        });
                    })
                })
        });

        
    }catch(error) {
        console.log(error);
        return {
            "success": false,
            "message": "There's something wrong with video uploading functionality"
        }
    }
    
}


const videoList = (req, res) => {
    try {
        const videos = Video.find({})
                            .sort({"createdAt": -1})
                            .exec((err, video) => {
                                if(err) {
                                    return {
                                        "success": false,
                                        "message": err
                                    }
                                }
                                res.status(200)
                                .send({
                                 "success": true,
                                 "message": "Successfully fetched the video lists.",
                                 "videos": video
                                });
                            })
    }catch(error) {
        console.log(error);
        return {
            "success": false,
            "message": "We have an issue with video fetching algorithm."
        }
    }
}

const likeaVideo = async (req, res) => {
    try {
        const videoId = req.body._id;
        const userId = req.body.userId;

        await Video.findOne({
            $and: [{
                _id: videoId
            }, {
                likers: {
                    _id: userId
                }
            }]
        }, async function(error, video) {
            if(error) {
                return console.log(error)
            }

            if(video == null) {
                const dislikersList = await Video.findById({
                    _id: videoId
                });
        
                await Video.updateOne({
                    _id: videoId,
                }, {
                    $pull: {
                        dislikers: { _id: userId }
                    },
                    $push: {
                        likers: { _id: userId }
                    }
                },
                {new: true, runValidators: true},
                async function(error2, data) {
                    if(error2) {
                        return console.log(error)
                    }
        
                    return res.send({
                        "success": true,
                        "message": "You liked this video!",
                        "likersData": video,
                        "dislikersData": dislikersList
                    })
                }).clone().catch(function(err) { console.log(err) })
            }else{
                console.log("I already liked")
                return res.send({
                    "success": true,
                    "code": "REPLICA_LIKERS_ID",
                    "message": "You already liked this video."
                })
            }
        }).clone().catch(function(err) { console.log(err) });
    }catch(error) {
        console.log(error);
        return {
            "success": false,
            "message": "There's something with likes functionality"
        }
    }
}

const dislikeaVideo = async(req, res) => {
    try {
        const videoId = req.body._id;
        const userId = req.body.userId;

        await Video.findOne({
            $and: [{
                _id: videoId
            }, {
                dislikers: {
                    _id: userId
                }
            }]
        }, async function(error, video) {
            if(error) {
                return console.log("dislikes line: 212", error)
            }

            if(video == null) {
                const likersList = await Video.findById({
                    _id: videoId
                });
        
                await Video.updateOne({
                    _id: videoId,
                }, {
                    $pull: {
                        likers: { _id: userId }
                    },
                    $push: {
                        dislikers: { _id: userId }
                    }
                },
                {new: true, runValidators: true},
                async function(error, data) {
                    if(error) {
                        return console.log("dislike line 233: ", error);
                    }
                    return res.send({
                        "success": true,
                        "message": "You disliked this video!",
                        "dislikersData": video,
                        "likersData": likersList
                    })
                }).clone().catch(function(err) { console.log(err) });
            }else{
                console.log("I already disliked")
                return res.send({
                    "success": true,
                    "code": "REPLICA_DISLIKERS_ID",
                    "message": "You already disliked this video."
                })
            }
        }).clone().catch(function(err) { console.log(err) });
    }catch(error) {
        console.log(error);
        return {
            "success": false,
            "message": "There's something with dislikes functionality"
        }
    }
}

const insertComment = async(req, res) => {
    let newId = new mongoose.Types.ObjectId();

    try {
        getUser(req.body.userId, async function(user) {
            await Video.findOneAndUpdate({
                _id: req.body._id
            }, {
                $push: {
                    "comments": {
                        _id: newId,
                        user: {
                            _id: user._id,
                            name: user.name,
                            image: user.image
                        },
                        comment: req.body.comment,
                        createdAt: new Date().getTime(),
                        replies: []
                    }
                }
            })

            return res.status(201)
                .send({
                "success": true,
                "message": "Comment has been posted",
                "commentId": newId
            })
        })
    }catch(error) {
        console.log(error);
        return res.send({
            "success": false,
            "message": "There's something wrong with comment functionality"
        })
    }
}

const insertReply = (req, res) => {
    try {
        const reply = req.body.reply;
        const commentId = req.body._id;
        const userId = req.body.userId;
        let newId = new mongoose.Types.ObjectId();
        getUser(userId, async function(user) {
            await Video.findOneAndUpdate({
                "comments._id": commentId
            }, {
                $push: {
                    "comments.$.replies": {
                        _id: newId,
                        user: {
                            _id: user._id,
                            name: `${user.firstName} ${user.lastName}`,
                            image: user.image
                        },
                        reply: reply,
                        createdAt: new Date().getTime()
                    }
                }
            });
            res.send({
                "success": true,
                "message": "Successfully inserted a reply.",
                "replyId": newId
            })
        });
    }catch(error) {
        console.log(error);
        return res.send({
            "success": false,
            "message": "There's something wrong with reply functionality"
        })
    }
}

const relatedMovie = async(req, res) => {
    const category = req.query.category;
    const userId = req.query.userId;
    const videoId = req.query._id;

    try {
        const relatedVideos = [];
        const RELATED_VIDS_WITH_SAME_CATEGORY = await Video.aggregate([
            {
                '$match': {
                'category': category
                }
            }
            ]);

        const RELATED_VIDS_WITH_FROM_USER = await Video.aggregate([
            {
              '$match': {
                'user._id': mongoose.Types.ObjectId(userId), 
                'category': category
              }
            }
          ]);

          RELATED_VIDS_WITH_SAME_CATEGORY.map(relatedVidsWithSameCategory => relatedVideos.push(relatedVidsWithSameCategory));
          RELATED_VIDS_WITH_FROM_USER.map(relatedVidsWithFromUser => relatedVideos.push(relatedVidsWithFromUser));
            
        //   const filteredVideos = relatedVideos.filter(video => video._id !== new mongoose.Types.ObjectId(videoId) && video);
          for(let i = 0; i < relatedVideos.length; i++) {
            if(relatedVideos[i].title === req.query.title) {
                relatedVideos.splice(i, 1);
                
            } 
          }

          res.send({
            "success": true,
            "message": "Successfully fetched the related videos",
            relatedVideos: relatedVideos
          });

    }catch(error) {
        console.log(error);
        return res.send({
            "success": false,
            "message": "There's somethign wrong with getting related videos"
        })
    }
}

const updateViews = async (req, res) => {
    try {
        const videoId = req.body._id;
        const userId = req.body.userId;

        await User.updateOne({
                $and: [{
                    "_id": mongoose.Types.ObjectId(userId),
                }, {
                    "videos._id": mongoose.Types.ObjectId(videoId)
                }]
        }, {
            $inc: { "videos.$.views": 1 }
        }, {returnOriginal: false});

        const video = await Video.findOneAndUpdate({
            _id: mongoose.Types.ObjectId(videoId)
        }, {
            $inc: { views: 1 }
        });

        return res.send({
            success: true,
            message: `You watch ${video.title}`
        })
    }catch(error) {
        console.log(error);
        return res.send({
            success: false,
            message: "We have an issue with updating the video views"
        })
    }

}

const updateVideoHistory = async (req, res) => {
    const newId = new mongoose.Types.ObjectId()
    const videoId = req.body._id;
    const video_watched_duration = req.body.watchedTime;
    const userId = req.body.userId;
    try {
        const video = await Video.findOne({
            _id: videoId
        });
    
        const hasVideoHistory = await User.findOne({
            $and: [{
                _id: mongoose.Types.ObjectId(userId)
            }, {
                "history.videoId": mongoose.Types.ObjectId(video)
            }]
        });
    
        if(hasVideoHistory === null) {
            await User.updateOne({
                _id: mongoose.Types.ObjectId(userId)
            }, {
                $push: {
                    "history": {
                        _id: newId,
                        videoId: video._id,
                        watch: video.watch,
                        title: video.title,
                        watched: video_watched_duration,
                        thumbnail: video.thumbnail,
                        hours: video.hours,
                        minutes: video.minutes,
                        seconds: video.seconds
                    }
                }
            });

            return res.send({
                success: true,
                message: "Video history updated.",
                newId: newId,
                new: true
            })
        }else{
            await User.updateOne({
                $and: [{
                    _id: mongoose.Types.ObjectId(userId)
                }, {
                    "history.videoId": mongoose.Types.ObjectId(video)
                }]
            }, {
                $set: {
                    "history.$.watched": video_watched_duration
                }
            });

            return res.send({
                success: true,
                message: "Video history updated.",
                newId: newId,
                new: false
            })
        }
    }catch(error) {
        console.log(error);
        return res.send({
            success: false,
            message: "There's something wrong with adding video to history"
        })
    }
}

const updateVideo = async (req, res) => {
    try {
        const { videoId,
            userId,
            title,
            description,
            tags,
            category,
            video,
            thumbnail,
            wallpaper,
            year,
            playlist,
            isfeatured
        } = req.body;

        if(isfeatured) {
            await Video.updateMany({}, {
                $set: {
                    isFeatured: false
                }
            });
        }

        // Updating the actual video
        const updatedVideo = await Video.findOneAndUpdate({
            _id: mongoose.Types.ObjectId(videoId)
        }, {
            $set: {
                title: title,
                description: description,
                tags: tags,
                category: category,
                video: video,
                thumbnail: thumbnail,
                wallpaper: wallpaper,
                year: year,
                album: playlist,
                isFeatured: isfeatured
            }
        });

        console.log("Update processing....");
        if(playlist === "") {
            console.log("Update dont have playlist request....")
            // Updating users video
            await User.findOneAndUpdate({
                $and: [{
                    _id: mongoose.Types.ObjectId(userId)
                }, {
                    "videos._id": mongoose.Types.ObjectId(videoId)
                }]
            },{
                $set: {
                    "videos.$.title": title,
                    "videos.$.thumbnail": thumbnail
                }
            });


            return res.send({
                success: true,
                message: "Done updating the video!"
            });
        }else{
            console.log("Update has playlist request....")
            if(updatedVideo.album !== "") {
                await User.updateOne({
                    $and: [{
                        _id: mongoose.Types.ObjectId(userId)
                    }, {
                        "playlists._id": mongoose.Types.ObjectId(playlist)
                    }]
                }, {
                    $pull: {
                        "playlists.$.video": {
                            _id: mongoose.Types.ObjectId(playlist)
                        }
                    }
                });

                await User.updateOne({
                    $and: [{
                        _id: mongoose.Types.ObjectId(userId)
                    }, {
                        "playlists._id": mongoose.Types.ObjectId(playlist)
                    }]
                }, {
                    $push: {
                        "playlists.$.video": updatedVideo
                    }
                });

                await User.findOneAndUpdate({
                    $and: [{
                        _id: mongoose.Types.ObjectId(userId)
                    }, {
                        "videos._id": mongoose.Types.ObjectId(videoId)
                    }]
                },{
                    $set: {
                        "videos.$.title": title,
                        "videos.$.thumbnail": thumbnail
                    }
                });

                return res.send({
                    success: true,
                    message: "Done updating the video!"
                });
            }
        }
    
    }catch(error) {
        console.log(error);
        return res.send({
            success: false,
            message: "There's something wrong with video update functionality."
        })
    }
}

const deleteVideo = async (req, res) => {

    try {
        const {
            userId,
            videoId
        } = req.body;

        const isUsersVideo = await Video.findOne({
            $and: [{
                _id: mongoose.Types.ObjectId(videoId)
            }, {
                "user._id": mongoose.Types.ObjectId(userId)
            }]
        });
    
        if(isUsersVideo === null) {
            return res.send({
                success: false,
                message: "Unable to process request. This is not your video"
            })
        }else{
            // Delete the video
            await Video.findOneAndDelete({
                $and: [{
                    _id: mongoose.Types.ObjectId(videoId)
                }, {
                    "user._id": mongoose.Types.ObjectId(userId)
                }]
            });

            // Users deleted the video that he own
            await User.findOneAndUpdate({
                _id: mongoose.Types.ObjectId(userId)
            }, {
                $pull: {
                    "videos": {
                        "_id": mongoose.Types.ObjectId(videoId)
                    }
                }
            });

            // Delete the same video to all users history
            await User.updateMany({},
                {
                    $pull: {
                        "history": {
                            "videoId": mongoose.Types.ObjectId(videoId)
                        }
                    }
                }
            );

            res.send({
                success: true,
                message: `You successfully deleted the videos with id number: ${videoId}`
            })

        }
    }catch(error) {
        console.log(error);
        return res.send({
            success: false,
            message: "There's something wrong with delete functionality."
        });
    }

    
}

const featuredVideo = async(req, res) => {
    try {
        const featured = await Video.aggregate([
            {
              '$sample': {
                'size': 1
              }
            }
        ]);

        return res.send({
            success: true,
            message: "Featured video of the day!",
            featuredVideo: featured,
            splitTags: featured[0].tags.split(", ").slice(0, 3)
        });
        
    }catch(error) {
        console.log(error);
        return res.send({
            success: false,
            message: "There's something wrong with featured video."
        })
    }
}

const playlistCreation = async(req, res) => {
    try {
        const { userId, thumbnail, title, year } = req.body;
        const newId = new mongoose.Types.ObjectId();
        
        await User.updateOne({
            _id: mongoose.Types.ObjectId(userId)
        }, {
            $push: {
                playlists: {
                    _id: newId,
                    title: title,
                    thumbnail: thumbnail,
                    year: year,
                    video: []
                }
            }
        });

        return res.send({
            success: true,
            message: "You successfully created a playlist.",
            newId: newId
        });

    }catch(error) {
        console.log(error);
        return res.send({
            success: false,
            message: "There's something wrong with playlist creation"
        })
    }
}

const playlistDelete = async (req, res) => {
    try {
        const { userId, playlistId } = req.body;

        const isUsersPlaylist = await User.findOne({
            $and: [{
                _id: mongoose.Types.ObjectId(userId)
            }, {
                "playlists._id": mongoose.Types.ObjectId(playlistId)
            }]
        });

        if(isUsersPlaylist === null) {
            return res.send({
                success: false,
                message: "You cannot delete this as this is not your playlist"
            });
        }

        await User.updateOne({
            _id: mongoose.Types.ObjectId(userId)
        }, {
            $pull: {
                "playlists": {
                    _id: mongoose.Types.ObjectId(playlistId)
                }
            }
        });

        await Video.updateMany({
            playlist: playlistId
        }, {
            $set: {
                album: ""
            }
        });

        return res.send({
            success: true,
            message: `You successfully deleted the playlist.`
        })
    }catch(error) {
        console.log(error);
        return res.send({
            success: false,
            message: "Something wrong with playlist deletion functionality"
        })
    }
}

const subscribed = async(req, res) => {
    try {
        const { userId, videoId, videoOwner } = req.body;
        
        /*
            Check if I owned the video or do I subscribe already to the channel
            isVideoSubscribe is me
            videoOwner is user who owned the video
        */
        const isVideoSubscribe = await Video.findOne({
            _id: mongoose.Types.ObjectId(videoId)
        });

        const subscriber = await User.findOne({
            _id: userId
        })

        if(isVideoSubscribe.user._id.toString() === userId.toString()) {
            return res.send({
                success: true,
                code: 409,
                message: "You cannot subscribed your own video."
            });
        }

        getUser(videoOwner, async function(user) {
            let flag = false;
            for(let i = 0; i < user.subscriptions.length; i++) {
                const userData = user.subscriptions[i];
                if(userData._id.toString() === userId.toString()) {
                    console.log("flag is open");
                    flag = true;
                    break;
                }
            }

            if(flag) {
                return res.send({
                    success: true,
                    code: 209,
                    message: "You already subscribed on this channel."
                })
            }

            // Users who has video uploaded
            const successfullyUpdatedVideoSubscribe = await Video.updateMany({
                "user._id": mongoose.Types.ObjectId(videoOwner)
            }, {
                $inc: { "user.subscribers": 1 }
            });

            console.log(successfullyUpdatedVideoSubscribe);

            await User.updateOne({
                _id: mongoose.Types.ObjectId(videoOwner)
            }, {
                $push: {
                    subscriptions: {
                        _id: mongoose.Types.ObjectId(subscriber._id),
                        name: subscriber.firstName + " " + subscriber.lastName,
                        subscribers: subscriber.subscriptions.length + 1,
                        image: subscriber.image
                    }
                }
            });

            return res.send({
                success: true,
                code: 200,
                message: "You successfully subscribed on the channel.",
            });
        })
    }catch(error) {
       console.log(error);
       return res.send({
        success: false,
        message: "There's something wrong with subscribe functionality."
       }) 
    }
}

const unsubscribed = async(req, res) => {
    try {
        const {
            channelOwner,
            subscriber            
        } = req.body;

        await User.updateOne({
            _id: mongoose.Types.ObjectId(channelOwner)
        }, {
            $pull: {
                subscriptions: {
                    _id: mongoose.Types.ObjectId(subscriber)
                }
            }
        });

        await Video.updateMany({
            "user._id": mongoose.Types.ObjectId(channelOwner)
        }, {
            $inc: { "user.subscribers": -1 }
        });

        return res.send({
            success: true,
            message: "You unsubscribed to this channel."
        })
    }catch(error) {
        console.log(error);
        return res.send({
            success: false,
            message: "There's something wrong with unsubscribed."
        })
    }
}

module.exports = {
    createVideo,
    videoList,
    likeaVideo,
    dislikeaVideo,
    insertComment,
    insertReply,
    relatedMovie,
    updateViews,
    updateVideoHistory,
    updateVideo,
    deleteVideo,
    featuredVideo,
    playlistCreation,
    playlistDelete,
    subscribed,
    unsubscribed
}