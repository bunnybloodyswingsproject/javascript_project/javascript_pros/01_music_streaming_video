const router = require("express").Router();
const verifyJWT = require("../middlewares/verifyJWT");
const {
    refresh,
    findUsers,
    persistAuth,
    testFindUser,
    avatarUpdate,
    getUsers,
    updateUser
} = require("../controllers/user");

router.get("/refresh", refresh);
router.get("/", verifyJWT ,findUsers);
router.post("/persistingauth", persistAuth);
router.post("/avatarupdate", avatarUpdate);
router.get("/getusers", getUsers);
router.post("/user_management", updateUser);
module.exports = router;

