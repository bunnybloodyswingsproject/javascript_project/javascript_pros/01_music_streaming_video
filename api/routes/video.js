const router = require("express").Router();
const verifyJWT = require("../middlewares/verifyJWT");

const {
    createVideo,
    videoList,
    likeaVideo,
    dislikeaVideo,
    insertComment,
    insertReply,
    relatedMovie,
    updateViews,
    updateVideoHistory,
    updateVideo,
    deleteVideo,
    featuredVideo,
    playlistCreation,
    playlistDelete,
    subscribed,
    unsubscribed
} = require("../controllers/videos");

router.post("/upload", createVideo);
router.get("/list", videoList);
router.post("/likes", likeaVideo);
router.post("/dislikes", dislikeaVideo);
router.post("/add_comment", insertComment);
router.post("/reply", insertReply);
router.get("/related_movies", relatedMovie);
router.post("/update_views", updateViews);
router.post("/save_history", updateVideoHistory)
router.post("/update_video", updateVideo)
router.post("/delete_video", deleteVideo);
router.get("/featured_video", featuredVideo);
router.post("/create_playlist", playlistCreation);
router.post("/delete_playlist", playlistDelete);
router.post("/subscribe", subscribed);
router.post("/unsubscribe", unsubscribed);

module.exports = router;