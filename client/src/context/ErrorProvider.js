import { createContext, useState } from "react";

export const ErrorContext = createContext("");
export const ErrorProvider = ({children}) => {
    const [errMsg, setErrMsg] = useState("");

    return (
        <ErrorContext.Provider value={{errMsg, setErrMsg}}>
            {children}
        </ErrorContext.Provider>
    )
}