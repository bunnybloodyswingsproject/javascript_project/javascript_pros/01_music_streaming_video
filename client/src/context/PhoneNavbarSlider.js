import { createContext, useState } from 'react'

export const phoneSidebarContext = createContext(false);
export const PhoneSidebarProvider = ({children}) => {
    const [isPhoneSidebarOpen, setIsPhoneSidebarOpen] = useState(false)
    return (
        <phoneSidebarContext.Provider value={{isPhoneSidebarOpen, setIsPhoneSidebarOpen}}>
            {children}
        </phoneSidebarContext.Provider>
    )
}
