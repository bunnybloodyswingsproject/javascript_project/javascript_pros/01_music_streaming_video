import { createContext, useState } from "react";

export const SidebarContext = createContext(false);
export const SidebarProvider = ({children}) => {
    const [isSidebarOpen, setIsSideOpen] = useState(true);
    
    return (
        <SidebarContext.Provider value={{isSidebarOpen, setIsSideOpen}}>
            {children}
        </SidebarContext.Provider>
    )
}