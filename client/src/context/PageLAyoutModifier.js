import { createContext, useState } from "react";

export const PageLayoutContext = createContext(false);
export const LayoutProvider = ({children}) => {
    const [isWatchPage, setIsWatchPage] = useState(false);
    return (
        <PageLayoutContext.Provider value={{ isWatchPage, setIsWatchPage }}>
            { children }
        </PageLayoutContext.Provider>
    )
}