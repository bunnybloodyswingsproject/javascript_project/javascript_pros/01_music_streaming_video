import Register from "./pages/register/Register";
import "./App.css";
import Login from "./pages/login/login";
import {BrowserRouter as Router, Routes, Route} from "react-router-dom";
import { useEffect, useMemo, useRef, useState } from "react";
import {AuthContext} from "./context/AuthProvider";
import Home from "./pages/home/home";
import Error from "./pages/error/error";
import Layout from "./components/layout";
import RequireAuth from "./components/RequireAuth";
import VideoEdit from "./pages/Edit_Video/VideoEdit";
import AnotherPage from "./pages/AnotherPage";
import PersistLogin from "./components/PersistLogin";
import Upload from "./pages/Upload/Upload";
import Watch from "./pages/watch/Watch";
import UserProfile from "./pages/User_Page/UserProfile";
import History from "./pages/History/history";
import useSidebarOpen from "./hooks/useSidebarOpen";
import useWindowDimensions from "./hooks/useWindowDimensions";
import { connectStorageEmulator } from "firebase/storage";
function App() {

  const { height, width } = useWindowDimensions();
  const {isSidebarOpen, setIsSideOpen} = useSidebarOpen();
  useMemo(() => {
    width <= 1024 ? setIsSideOpen(false) : setIsSideOpen(true);
    width <= 500 ? setIsSideOpen(true) : setIsSideOpen(false)
  }, [width, setIsSideOpen]);
  
  return (
    <div className="App">
      <Router>
        <Routes>
          <Route path="/" element={<Layout />}>
            {/* Public Route */}
            <Route path="register" element={<Register />} />
            <Route path="login" element={<Login />} />
            <Route path="*" element={<Error />} />
            
            {/* Restricted Routes */}
            <Route element={<PersistLogin />}>
              <Route element={<RequireAuth />}>
                  <Route path="/edit_video" element={<VideoEdit />} />
                  <Route path="/anotherpage" element={<AnotherPage />} />
                  <Route path="/" element={<Home />} />
                  <Route path="/playlist" element={<AnotherPage />} />
                  <Route path="/settings" element={<Upload />} />
                  <Route path="/watch" element={<Watch />} />
                  <Route path="/user_profile" element={<UserProfile />} />
                  <Route path="/history" element={<History />} />
              </Route>
            </Route>
          </Route>
        </Routes>
      </Router>
    </div>
  );
}

export default App;
