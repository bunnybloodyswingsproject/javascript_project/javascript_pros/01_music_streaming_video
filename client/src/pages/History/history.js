import { NavLink, useLocation } from "react-router-dom"
import "./history.css"

const History = () => {
    const location = useLocation();
    const user = location?.state?.user;
    const videos = location?.state?.videos;    
    return (
    <div className="streaming__subpages">
        <div className="streaming__userProfile">
            <div className="streaming__userProfile_wallpaper">
                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTKt9pNzdBn7o1MOg1BiNkWtpQy6xnh14i9FpIiZBW6vTwVTK1EqjCX1bvWHL52iANAuQA&usqp=CAU" alt="users_wallpaper" />
            </div>
            <div className="streaming__userProfile_infoTabs">
                <div className="streaming_userProfile_infoTabs_tabphane">
                <div className="streaming_userProfile_infoTabs_tabphane">
                    <NavLink className={({isActive}) => (
                        isActive ? "tabphane_active_link" : ""
                    )} to="/user_profile"
                    state={{ 
                        user: user, 
                        videos: videos
                    }}
                    ><span>Profile</span></NavLink>
                    <NavLink className={({isActive}) => (
                        isActive ? "tabphane_active_link" : ""
                    )} to="/history"><span>History</span></NavLink>
                    <NavLink className={({isActive}) => (
                        isActive ? "tabphane_active_link" : ""
                    )} to="/playlist"><span>Playlist</span></NavLink>
                    <NavLink className={({isActive}) => (
                        isActive ? "tabphane_active_link" : ""
                    )} to="/subscribed_channels"><span>Subscribed Channels</span></NavLink>
                </div>
                </div>
                <div className="streaming_userProfile_infoTabs_TB_category_container">
                    <div className="streaming_userProfile_infoTabs_TB_featured">
                        <div className="streaming_userProfile_infoTabs_TB_featured_video">
                            <video src="https://firebasestorage.googleapis.com/v0/b/netflix-f5fc3.appspot.com/o/batugst_file_uploader%2F1668834671582videoMarvel_Studios%E2%80%99_Secret_Invasion___Official_Trailer___Disney%2B.mp4?alt=media&token=db3afbb7-7471-4ee5-9037-c66bc09af71b" 
                                    autoPlay  
                                    controls
                            />
                        </div>
                        <div className="streaming_userProfile_infoTabs_TB_featured_description">
                            <h5>True Sight: The International 2021</h5>
                            <small className="description__video_owner">Jenkins Waiter</small>
                            <small className="description__video_description">
                                From Regional Qualifier hopefuls to champions at The International in Bucharest — the world got to witness Team Spirit's ascendance play out on Dota's biggest stage. Go behind the scenes for the final battles of that epic coronation in an all-new TrueSight premiering September 24.
                            </small>
                        </div>
                        <hr />
                        <div className="streaming_userProfile_infoTabs_TB_featured_user_uploads">
                            <h5>Uploads</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    )
}

export default History