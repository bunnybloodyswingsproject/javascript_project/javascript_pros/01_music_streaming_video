import React from 'react'
import "./login.css";
import Navbar from "../../components/authNavbar";
import {useRef, useEffect, useState} from "react";
import axios from "axios";
import useAuth from "../../hooks/useAuth";
import { Link, useNavigate, useLocation } from 'react-router-dom';

const Login = () => {
  const {setUser} = useAuth();
  const emailLabel = useRef();
  const passwordLabel = useRef();
  const errRef = useRef();

  const [email, setEmail] = useState("");
  const [pwd, setPwd] = useState("");
  const [errMsg, setErrMsg] = useState("");
  
  const navigate = useNavigate();
  const location = useLocation();
  const from = location.state?.from?.pathname || "/";
  const axiosInstance = axios.create({
    baseUrl: process.env.REACT_APP_API_URL
  });
  useEffect(() => {
    emailLabel.current.focus();
  }, []);

  useEffect(() => {
    setErrMsg("");
  }, [email, pwd])

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      const response = await axiosInstance.post("auth/login",
      JSON.stringify({
        email,
        password: pwd
      }), {
        headers: { "Content-Type": "application/json" },
        withCredentials: true
      });
      
      if(!response.data.success) {
        setErrMsg(response.data.message);
      }

      setUser(response.data);
      emailLabel.current.value = ""
      passwordLabel.current.value = ""
      navigate(from, { replace: true });
      
    }catch(error) {
      console.log(error);
      if(!error?.response || error.status === 500) {
        setErrMsg("No Server Response");
      }else{
        setErrMsg("Registration Failed");
        console.log(error);
      }
      errRef.current.focus();
    }
  }
  
  return (
    <div className="register">
      <Navbar />
      <div className='autform__content'>
        <div className='authform'>
          <div className="auth__title">
            <p>Experience the awesome streaming</p>
            <div className="auth__banner_text">
              <h1>Welcome</h1>
              <h1>.</h1>
            </div>
            <small>Not A member? <Link to="/register">Sign up</Link></small>
            <div ref={errRef} 
                  className={errMsg ? "errmsg" : "offscreen"}
                  aria-live="assertive"
            > <i className="fa-solid fa-triangle-exclamation"></i> {errMsg}</div>
          </div>
          <div className="l-form">
            <form className="form" onSubmit={handleSubmit}>
              <div className="form_div">
                <input type="text" 
                        className="form_input" 
                        placeholder=" " 
                        name="email"
                        autoComplete="new-email" 
                        id="email"
                        onChange={(e) => setEmail(e.target.value)}
                        ref={emailLabel}
                        required
                        value={email}
                />
                <label htmlFor='email' className='form_lable' onClick={() => emailLabel.current.focus()}>Email</label>
              </div>
              <div className="form_div">
                <input type="password" 
                        id="password"
                        className="form_input" 
                        placeholder=" " 
                        name="password"
                        autoComplete="new-password"  
                        ref={passwordLabel}
                        onChange={(e) => setPwd(e.target.value)}
                        required
                        value={pwd}

                />
                <label htmlFor='password' className='form_lable' onClick={() => passwordLabel.current.focus()}>Password</label>
              </div>
              <button className="form_button">Login</button>
            </form>
          </div>
        </div>
        <div className="auth__logo_banner">
          <img  src="https://firebasestorage.googleapis.com/v0/b/practicefreewebsitehosting.appspot.com/o/Form%20logos.png?alt=media&token=7101c15e-f2fd-4851-865b-58d1c7bbf373" alt="auth__logo_banner" />
        </div>
      </div>
    </div>
  )
}

export default Login