import React from 'react'
import "./register.css";
import { Link, useNavigate, useLocation } from 'react-router-dom';
import Navbar from "../../components/authNavbar";
import {useRef, useEffect, useState} from "react";
import axios from "axios";

const Register = () => {
  const fNameLabel = useRef();
  const lNameLabel = useRef();
  const emailLabel = useRef();
  const passwordLabel = useRef();
  const confirmPasswordLabel = useRef();
  const errRef = useRef();
  const location = useLocation();
  const navigate = useNavigate();
  const from = location.state?.from?.pathname || "/login";

  const USER_REGEX = /^[a-zA-Z][a-zA-Z0-9]{3,23}$/;
  const PASS_REGEX = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%]).{5,50}$/;
  const EMAIL_REGEX = /^(?=.*[a-z])(?=.*[0-9])(?=.*[!@#$%.]).{12,50}$/;

  const [firstName, setFirstName] = useState("");
  const [validFName, setValidFName] = useState(false);
  const [firstNameFocus, setFirstNameFocus] = useState(false);

  const [lastName, setLastName] = useState("");
  const [validLName, setValidLName] = useState(false);
  const [lastNameFocus, setLastNameFocus] = useState(false);

  const [email, setEmail] = useState("");
  const [validEmail, setValidEmail] = useState(false);
  const [emailFocus, setEmailFocus] = useState(false);

  const [pwd, setPwd] = useState("");
  const [validPwd, setValidPwd] = useState(false);
  const [PwdFocus, setPwdFocus] = useState(false);
  
  const [matchPwd, setMatchPwd] = useState("");
  const [validMatchPwd, setValidMatchPwd] = useState(false);
  const [matchPwdFocus, setMatchPwdFocus] = useState(false);

  const [errMsg, setErrMsg] = useState("");
  const [success, setSuccess] = useState(false);
  
  useEffect(() => {
    fNameLabel.current.focus();
  }, []);

  useEffect(() => {
    const result = USER_REGEX.test(firstName);
    setValidFName(result);
  }, [firstName]);

  useEffect(() => {
    const result = USER_REGEX.test(lastName);
    setValidLName(result);
  }, [lastName]);

  useEffect(() => {
    const result = EMAIL_REGEX.test(email);
    setValidEmail(result);
  }, [email]);


  useEffect(() => {
    const result = PASS_REGEX.test(pwd);
    setValidPwd(result);
    const match = pwd === matchPwd;
    setValidMatchPwd(match);
  }, [pwd, matchPwd]);

  useEffect(() => {
    setErrMsg("");
  }, [firstName, lastName, pwd, matchPwd])

  const handleSubmit = async (e) => {
    e.preventDefault();
    const firstNameVerifier = USER_REGEX.test(firstName);
    const lastNameVerifier = USER_REGEX.test(lastName);
    const emailVerifier = EMAIL_REGEX.test(email);
    const axiosInstance = axios.create({
      baseUrl: process.env.REACT_APP_API_URL
    });

    if(!firstNameVerifier || !lastNameVerifier || !emailVerifier) {
      setErrMsg("Invalid Entry");
      return;
    }

    try {
      const response = await axiosInstance.post("auth/register",
      JSON.stringify({
        firstName,
        lastName,
        email,
        password: pwd
      }), {
        headers: { "Content-Type": "application/json" },
        withCredentials: true
      });

      if(!response.data.success) {
        setErrMsg(response.data.message);
      }
      fNameLabel.current.value = ""
      lNameLabel.current.value = ""
      emailLabel.current.value = ""
      passwordLabel.current.value = ""
      confirmPasswordLabel.current.value = ""
      navigate(from, {replace: true })
    }catch(error) {
      
      if(!error?.response || error.status === 500) {
        setErrMsg("No Server Response");
      }else{
        setErrMsg("Registration Failed");
        console.log(error);
      }
      errRef.current.focus();
    }
  }

  return (
    <div className="register">
      <Navbar />
      <div className='autform__content'>
        <div className='authform'>
          <div className="auth__title">
            <p>START FOR FREE</p>
            <div className="auth__banner_text">
              <h1>Create new account</h1>
              <h1>.</h1>
            </div>
            <small>Already A member? <Link to="/login">Log in</Link></small>
            <div ref={errRef} 
                 className={errMsg ? "errmsg" : "offscreen"}
                 aria-live="assertive"
            > <i className="fa-solid fa-triangle-exclamation"></i> {errMsg}</div>
          </div>
          <div className="l-form">
            <form className="form" onSubmit={handleSubmit}>
              <div className="auth__fullname_inputs">
                <div className="form_div">
                  <input type="text" 
                         className="form_input"
                         id="username"
                         autoComplete="new-firstname"
                         placeholder=" " 
                         name="firstname" 
                         ref={fNameLabel} 
                         onChange={e => setFirstName(e.target.value)}
                         required
                         aria-invalid={validFName ? "false" : "true"}
                         aria-describedby="uidnote"
                         onFocus={() => setFirstNameFocus(true)}
                         onBlur={() => setFirstNameFocus(false)}
                  />
                  <label htmlFor='firstname' className='form_lable' onClick={() => fNameLabel.current.focus()}>First Name</label>
                </div>
                <div className="form_div">
                  <input type="text" 
                         className="form_input" 
                         placeholder=" " 
                         name="lastname"
                         autoComplete="new-lastname" 
                         ref={lNameLabel}
                         onChange={e => setLastName(e.target.value)}
                         required
                         aria-invalid={validLName ? "false" : "true"}
                         aria-describedby="uidnote"
                         onFocus={() => setLastNameFocus(true)}
                         onBlur={() => setLastNameFocus(false)}
                  />
                  <label htmlFor='lastname' className='form_lable' onClick={() => lNameLabel.current.focus()}>Last Name</label>
                </div>
              </div>
              <p id="uidnote" className={(firstName && !validFName) || (lastName && !validLName) ? "instructions" : "offscreen"}>
              <i className="fa-solid fa-circle-info"></i>
              4 to 24 characters. <br/>
              Must begin with a letter. <br />
              numbers and underscores are not allowed
              </p>
              <div className="form_div">
                <input type="text" 
                       className="form_input" 
                       placeholder=" " 
                       name="email"
                       autoComplete="new-email" 
                       id="email"
                       onChange={(e) => setEmail(e.target.value)}
                       ref={emailLabel}
                       required
                       aria-invalid={validEmail ? "false" : "true"}
                       aria-describedby="emailnote"
                       onFocus={() => setEmailFocus(true)}
                       onBlur={() => setEmailFocus(false)}
                       disabled={validFName && validLName ? false : true}
                />
                <label htmlFor='email' className='form_lable' onClick={() => emailLabel.current.focus()}>Email</label>
              </div>
              <p id="emailnote" className={email && !validEmail ? "instructions" : "offscreen"}>
              <i className="fa-solid fa-circle-info"></i>
              11 to 24 characters. <br/>
              Must begin with a letter. <br />
              numbers and underscores are not allowed <br />
              should include @ .com, .net .org etc...
              </p>
              <div className="form_div">
                <input type="password" 
                       id="password"
                       className="form_input" 
                       placeholder=" " 
                       name="password"
                       autoComplete="new-password"  
                       ref={passwordLabel}
                       onChange={(e) => setPwd(e.target.value)}
                       required
                       aria-invalid={validPwd ? "false" : "true"}
                       aria-describedby="pwdnote"
                       onFocus={() => setPwdFocus(true)}
                       onBlur={() => setPwdFocus(false)}
                       disabled={validFName && validLName && validEmail ? false : true}

                />
                <label htmlFor='password' className='form_lable' onClick={() => passwordLabel.current.focus()}>Password</label>
              </div>
              <p id="pwdnote" className={pwd && !validPwd ? "instructions" : "offscreen"}>
              <i className="fa-solid fa-circle-info"></i>
              5 to 24 characters. <br/>
              Must include uppercase and lowercase letters, a number and a special character <br />
              Allowed special characters: <span aria-label="exclamation mark">!</span>
              <span aria-label="at symbol"> @</span> <span aria-label='hashtag'># </span> 
              <span aria-label='dollar sign'>$</span> <span aria-label='percent'>%</span>
              </p>
              <div className="form_div">
                <input type="password"
                       className="form_input"
                       id="confirmPassword"
                       placeholder=" "
                       autoComplete="new-confirmpassword" 
                       name="confirmPassword" 
                       ref={confirmPasswordLabel}
                       onChange={(e) => setMatchPwd(e.target.value)}
                       required
                       aria-invalid={validMatchPwd ? "false" : "true"}
                       aria-describedby="confirmnote"
                       onFocus={() => setMatchPwdFocus(true)}
                       onBlur={() => setMatchPwdFocus(false)}
                       disabled={validFName && validLName && validEmail && validPwd ? false : true}
                />
                <label htmlFor='confirmPassword' className='form_lable' onClick={() => confirmPasswordLabel.current.focus()}>Confirm Password</label>
              </div>
              <p id="confirmnote" className={matchPwdFocus && !validMatchPwd ? "instructions" : "offscreen"}>
              <i className="fa-solid fa-circle-info"></i>
              Password and confirm password is not match.
              </p>
              <button className={!validFName || !validLName || !validPwd || !validMatchPwd || !validEmail ? "form_button-disabled" : "form_button"} disabled={!validFName || !validLName || !validPwd || !validMatchPwd || !validEmail ? true : false}>Sign up</button>
            </form>
          </div>
        </div>
        <div className="auth__logo_banner">
          <img  src="https://firebasestorage.googleapis.com/v0/b/practicefreewebsitehosting.appspot.com/o/Form%20logos.png?alt=media&token=7101c15e-f2fd-4851-865b-58d1c7bbf373" alt="auth__logo_banner" />
        </div>
      </div>
    </div>
  )
}

export default Register