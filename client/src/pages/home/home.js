import React, { useEffect, useRef, useState } from 'react'
import { Link, useLocation } from 'react-router-dom'
import "./home.css";
import useSidebarOpen from "../../hooks/useSidebarOpen";
import MusicCard from '../../components/MusicCard';
import HomeController from './controller/HomeController';
import useAxiosPrivate from '../../hooks/useAxiosPrivate';
import useVideo from "../../hooks/useVideo";
import useLayout from '../../hooks/useLayout';
import usePhoneSidebarOpen from '../../hooks/usePhoneSidebarOpen';
const Home = () => {
  const {
    titleSubstringCounter,
    featuredBannerSubstringCounter,
    pageAnalysis
  } = HomeController()
  const {isSidebarOpen} = useSidebarOpen();
  const [isHovered, setIsHovered] = useState(false);
  const {videos, setVideos} = useVideo();
  const axiosPrivate = useAxiosPrivate();
  const effectRan = useRef(null);
  const location = useLocation().pathname;
  const { setIsWatchPage } = useLayout();
  const { setIsPhoneSidebarOpen } = usePhoneSidebarOpen();
  const months = [
      "January", 
      "February", 
      "March",
      "April", 
      "May", 
      "June", 
      "July", 
      "August", 
      "September", 
      "October",
      "November",
      "December",
    ]
  const thisMonth = new Date().getMonth();
  const thisYear = new Date().getFullYear();
  const [featuredVideo, setFeaturedVideo] = useState([]);
  const [tags, setTags] = useState([]);

  useEffect(() => {
    let isMounted = false;
    !isMounted && setIsWatchPage(pageAnalysis(location));

    return () => isMounted = true;
  }, [location, setIsWatchPage, pageAnalysis])

  useEffect(() => {
    const controller = new AbortController();
    let isMounted = true;

    const getVideos = async () => {
      try {
        const response = await axiosPrivate.get("videos/list", {
          signal: controller.signal
        });
        isMounted && setVideos(response.data.videos);
      }catch(error) {
        console.log(error)
      }
    }

    effectRan && getVideos();

    return () => {
      isMounted = false;
      controller.abort();
      effectRan.current = true;
    }
  }, [axiosPrivate, setVideos, effectRan]);

  useEffect(() => {
    let isMounted = false;
    const controller = new AbortController();

    const getFeaturedVideos = async () => {
  
      try {
        const response = await axiosPrivate.get("videos/featured_video", {
          signal: controller.signal
        }, {
          headers: { "Content-Type": "application/json" },
          withCredentials: true
        });
  
        if(response.data.success) {
          setFeaturedVideo(response.data.featuredVideo[0])
          setTags(response.data.splitTags);
        }

      }catch(error) {
        console.log(error)
      }
    }

    !isMounted && getFeaturedVideos()
    
    return () => {
      isMounted = true;
      controller.abort();
    }
  }, []);

  const showNavbarSlider = (e) => {
    setIsPhoneSidebarOpen(prev => !prev);
  }
  
  return (
    <>
      <div className="streaming__subpages">
        <div className="streaming__home" 
          style={{
            backgroundImage: `url(${featuredVideo.wallpaper || "https://wallpapercrafter.com/sizes/2048x1152/7056-texture-black-background-4k.jpg"})`
          }}
        >
          <div className="streaming_home_burger_icons">
            <i className="fa-solid fa-bars" onClick={showNavbarSlider}></i>
          </div>
          <div className="streaming__home_gradient_right"></div>
          <div className="streaming__home_banner_callToActions">
            <div className="streaming__home_banner_CTA_right">
              <small>{`Top Trending this ${months[thisMonth]} ${thisYear}`}</small>
              <h4>{featuredVideo.title || "Coming Soon..."}</h4>
              <div className="streaming__home_banner_CTAR_genre">
                {
                  tags?.map((tag, i) => (
                    <span key={i}>{tag}</span>
                  ))
                }
              </div>
            </div>
            <div className={isSidebarOpen ? "streaming__home_banner_callToActions_mid" : "streaming__home_banner_callToActions_mid streaming__home_banner_callToActions_mid_sidebarOff"}>
              <small>Descriptions</small>
              <p>{featuredBannerSubstringCounter(featuredVideo.description || "...")}</p>
            </div>
            <div className="streaming__home_banner_callToActions_left">
              <Link className="streaming__home_banner_CTRL_button"
                    to="/watch"
                    state={{ video: featuredVideo }}
              >Stream Now!</Link>
            </div>
          </div>
        </div>
        <div className="streaming__home_music_list">
          <div className='streaming__home_music_list_sectionTitle'>
            <h4>Streaming Now</h4>
          </div>
          <div className='streaming__home_music_list_music_container'>
            {
              videos?.length === 0
              ? (
                  <div className="streaming__home_music_list_empty_container">
                    <i className="fa-regular fa-folder-open"></i>
                    <p>Empty</p>
                  </div>
                )
              : (
                videos?.map((video, i) => (
                  <MusicCard key={i} index={i} videos={videos} video={video} isHovered={isHovered} setVideos={setVideos} setIsHovered={setIsHovered} titleSubstringCounter={titleSubstringCounter} />
                ))
              )
            }
          </div>
        </div>
      </div>
    </>
  )
}

export default Home