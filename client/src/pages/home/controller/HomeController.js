

const HomeController = () => {
    const titleSubstringCounter = (title) => {
        return title.length > 36 ? title.substring(0,30) + "..." : title; 
    }

    const featuredBannerSubstringCounter = (banner) => {
        return banner.length > 79 ? banner.substring(0,79) + "..." : banner; 
    }

    const pageAnalysis = (location) => {
        return location === "/watch" ? true : false;
    }    

    return {
        titleSubstringCounter,
        featuredBannerSubstringCounter,
        pageAnalysis
    }
}

export default HomeController