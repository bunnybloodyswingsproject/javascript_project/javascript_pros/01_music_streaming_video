import React, { useEffect, useRef, useState } from 'react'
import { Link, NavLink, useLocation } from 'react-router-dom'
import UploadForm from '../../components/UploadForm';
import UserManagementForm from '../../components/UserManagementForm';
import "./upload.css";
import useAuth from '../../hooks/useAuth';
import UploadController from "./controller/UploadController";
import useLayout from '../../hooks/useLayout';
import usePhoneSidebarOpen from '../../hooks/usePhoneSidebarOpen';
import useWindowDimensions from '../../hooks/useWindowDimensions';

const Upload = () => {
  const {
    pageAnalysis
  } = UploadController();
  const location = useLocation().pathname;
  const { setIsWatchPage } = useLayout();
  const [currentTab, setCurrentTab] = useState("Upload");
  const [linkButtonToggle, setLinkButtonToggle] = useState(false);
  const [settingsTabLinks, setSettingsTabLinks] = useState([
    { 
      "name": "Upload"
    },
    { 
      "name": "User Management"
    }
  ]);
  const { setIsPhoneSidebarOpen } = usePhoneSidebarOpen();
  const {user, setUser} = useAuth();
  
  useEffect(() => {
    let isMounted = false;
    !isMounted && setIsWatchPage(pageAnalysis(location));

    return () => isMounted = true;
  }, [pageAnalysis, setIsWatchPage, location])

  const handleSettingsPage = (e) => {
    const pageID = e.target.id;
    setCurrentTab(pageID);

  }

  const { width } = useWindowDimensions();

  const closeNavbarRef = useRef(false);
  useEffect(() => {
      closeNavbarRef.current && location === "/settings" && setIsPhoneSidebarOpen(false);
      return () => closeNavbarRef.current = true;
  }, [location,  setIsPhoneSidebarOpen]);

  return (
    <div className="streaming__subpages">
      {
        width <= 376 &&
        (
          <div className="streaming__watch_navbar">
            <NavLink to="/" className="streaming__sidebar_brandname">
                <div className="streaming__sidebar_logo"></div>
                <h3 className="streaming__sidebar_logo_text">Batugs</h3>
            </NavLink>
            <div className="streaming__watch_search_engine_formInputs">
                <div className="streaming__watch_user_avatar">
                    <img 
                        src={user.image}
                        alt="user_avatar"
                        onError={(e) => {
                            e.target.attributes.src.value = "https://www.pngitem.com/pimgs/m/150-1503945_transparent-user-png-default-user-image-png-png.png"
                        }}
                    />
                </div>
            </div>
          </div>
        )
      }
        <div className="streaming_settings_uploader_container">
            <div className="streaming_settings_uploader_pageDiv_title" onClick={() => setLinkButtonToggle(prev => !prev)}>
              <div className="streaming_settings_uploader_currentLink">
                <div className='streaming_settings_uploader_title'>
                  <h4>{currentTab}</h4>
                </div>
                <i className="fa-solid fa-caret-down" onClick={() => setLinkButtonToggle(prev => !prev)}></i>
              </div>
              <div className={linkButtonToggle ? "streaming_settings_uploader_cL_dropdownList openToggleLink" : "streaming_settings_uploader_cL_dropdownList closeToggleLink"}>
                {
                  settingsTabLinks.map((link, i) => (
                    <div key={i} id={link.name} className={link.name === currentTab ? "streaming_settings_uploader_CLD dropdownList_usm_active": "streaming_settings_uploader_CLD dropdownList_upload_active"} onClick={handleSettingsPage}>{link.name}</div>
                  ))
                }
              </div>
            </div>
            <div className="streaming__settings_uploader_content_container">
              {
                currentTab === "Upload" 
                ? (<UploadForm user={user} setUser={setUser} />)
                : (<UserManagementForm />)
              }
            </div>
        </div>
    </div>
  )
}

export default Upload