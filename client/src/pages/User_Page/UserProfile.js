import { NavLink, useLocation, useNavigate } from "react-router-dom";
import "./userProfile.css";
import user_profile_controller from "./controllers/user_profile.controller";
import MusicCard from "../../components/MusicCard";
import { useEffect, useRef, useState } from "react";
import useVideo from "../../hooks/useVideo";
import storage from "../../firebase";
import {ref, uploadBytesResumable, getDownloadURL} from "firebase/storage";
import PlaylistCard from "../../components/PlaylistCard";
import useAxiosPrivate from "../../hooks/useAxiosPrivate";
import useAuth from "../../hooks/useAuth";
import useLayout from "../../hooks/useLayout";
import SubscribedChannel from "../../components/SubscribedChannel";
import useWindowDimensions from "../../hooks/useWindowDimensions";
import useSidebarOpen from "../../hooks/useSidebarOpen";
import usePhoneSidebarOpen from "../../hooks/usePhoneSidebarOpen";
const UserProfile = () => {
    const axiosPrivate = useAxiosPrivate();
    const location = useLocation();
    const currentLocation = useLocation().pathname;
    const user = location?.state?.user;
    const videos = location?.state?.videos;
    const { setUser } = useAuth();
    const { setIsWatchPage } = useLayout();

    const {
        titleSubstringCounter,
        pageAnalysis,
        featuredDescriptionSubstringCounter
    } = user_profile_controller()
    const [isHovered, setIsHovered] = useState(false);
    const { setVideos } = useVideo();
    const [activeTab, setActiveTab] = useState("Profile");
    const [haveFeatured, setHaveFeatured] = useState(false);
    const [isActive, setIsActive] = useState([
        {
            id: 0,
            tab: "Profile",
            isActive: true
        },
        {
            id: 1,
            tab: "History",
            isActive: false
        },
        {
            id: 2,
            tab: "Playlist",
            isActive: false
        },
        {
            id: 3,
            tab: "Subscribed Channels",
            isActive: false
        }
    ]);
    
    let effectRan = useRef(false);
    let featuredEffectRan = useRef(false);
    const [usersHistory, setUsersHistories] = useState([]);
    const [renderPlaylistForm, setRenderPlaylistForm] = useState(false);
    const [playlistImgRenderer, setPlaylistImgRenderer] = useState(null);
    const [playlistTitle, setPlaylistTitle] = useState(null);
    const [year, setYear] = useState(null);
    const navigate = useNavigate();
    const { width } = useWindowDimensions();
    const { isSidebarOpen } = useSidebarOpen();
    const { setIsPhoneSidebarOpen } = usePhoneSidebarOpen();

    const handleActiveTab = (e) => {
        setActiveTab(e.target.id);
    }
    
    useEffect(() => {
        let isMounted = false;
        !isMounted && setIsWatchPage(pageAnalysis(currentLocation));
    
        return () => isMounted = true;
      }, [currentLocation, setIsWatchPage, pageAnalysis])

    useEffect(() => {
        if(effectRan.current) {
            for(let i = 0; i < user.history.length; i++) {
                for(let j = 0; j < videos?.length; j++) {
                    let history = user.history[i];
                    let video = videos[j];
    
                    if(history.videoId === video._id) {
                        setUsersHistories(prev => [
                            ...prev, 
                            video,
                        ]);
                        break;
                    }
                }
            }
        }

        return () => effectRan.current = true;
    }, [user.history, videos, effectRan]);

    useEffect(() => {
        if(featuredEffectRan.current) {
            for(let i = 0; i < videos?.length; i++) {
                if(videos[i].isFeatured) {
                    setHaveFeatured(true);
                    break;
                }
            }
        }

        return () => featuredEffectRan.current = true
    }, [videos]);

    const upload = (items) => {
        if(!items[0].file || items[0].type !== "image/png") {
            alert("Please upload a png file for your playlist")
        }

        items.forEach(item => {
            const filename = new Date().getTime() + item.label + item.file.name;
            const storageRef = ref(storage, "/batugst_file_uploader/" + filename);
            const uploadTask = uploadBytesResumable(storageRef, item.file);
            uploadTask.on("state_changed", (snapshot) => {
                let progress = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
                console.log(`Upload is ${progress}% done`);
                switch(snapshot.state) {
                    case "paused":
                        console.log("Upload is pause");
                        break;
                    case "running":
                        console.log("Upload is running");
                        break;
                    default:
                        console.log("Uploading the files....");
                        break;
                }
            },
            (err) => {console.log(err)},
            () => {
                getDownloadURL(uploadTask.snapshot.ref).then(url => {
                    setPlaylistImgRenderer(url);
                })
            })
        })
    }

    const onHandlePlaylistImgRenderer = (e) => {
        upload([{
            file: e.target.files[0]
        }])
    }
    const [errMsg, setErrMsg] = useState("");

    const onhandleCreatePlaylist = async () => {
        if(playlistImgRenderer === null || playlistTitle === null || playlistTitle === year) {
            return alert("No image renderer or title");
        }

        const response = await axiosPrivate.post("videos/create_playlist", 
            JSON.stringify({
                userId: user._id,
                title: playlistTitle,
                thumbnail: playlistImgRenderer,
                year: year
            }),
            {
                headers: { "Content-Type": "application/json" },
                withCredentials: true
            }
        )
        console.log(response)
        if(!response.data.success) {
            return setErrMsg(response.data.message);
        }

        setUser(prev => {
            return {
                ...prev,
                playlists: [
                    ...prev.playlists,
                    {
                        _id: response.data.newId,
                        title: playlistTitle,
                        thumbnail: playlistImgRenderer,
                        year: year,
                        video: []
                    }
                ]
            }
        });

        setPlaylistImgRenderer(null)
        setPlaylistTitle(null);
        setYear(null);
        setErrMsg("");
        setRenderPlaylistForm(prev => !prev);
        navigate("/");

    }

    const closeNavbarRef = useRef(false);
    useEffect(() => {
        closeNavbarRef.current && currentLocation === "/user_profile" && setIsPhoneSidebarOpen(false);
        return () => closeNavbarRef.current = true;
    }, [currentLocation,  setIsPhoneSidebarOpen])
    
    const showNavbarSlider = (e) => {
        setIsPhoneSidebarOpen(prev => !prev);
      }

    return (
    <div className="streaming__subpages">
        <div className="streaming__userProfile">
            <div className="streaming_userProfile_small_devices_navbar">
                <NavLink to="/" className="streaming_userProfile_brand">
                  <div className="streaming__sidebar_logo"></div>
                  <h3 className="streaming__sidebar_logo_text">Batugs</h3>
                </NavLink>
                <div className="streaming__sidebar_navbar_logo" onClick={showNavbarSlider}>
                    <div className="streaming__sidebar_navbar_logo_short"></div>
                    <div className="streaming__sidebar_navbar_logo_long"></div>
                </div>
            </div>
            <div className="streaming__userProfile_wallpaper">
                <img src={user.coverPhoto || "https://wallpaperaccess.com/full/1542111.jpg"} 
                     alt="users_wallpaper" 
                     onError={(e) => {
                        e.target.attributes.src.value = "https://capitoltheatre.com/wp-content/uploads/2017/03/placeholder-gc-1200x750.jpg"
                    }}
                />
            </div>
            <div className="streaming__userProfile_infoTabs">
                <div className="streaming_userProfile_infoTabs_tabphane">
                    {
                        isActive?.map((tab, i) => (
                            <div key={i} id={tab.tab} className={tab.tab === activeTab ? "tabphane_active_link" : ""}
                                 onClick={handleActiveTab}
                            ><span>{tab.tab}</span></div>
                        ))
                    }
                </div>
                <div className="streaming_userProfile_infoTabs_TB_category_container">
                    <div className="streaming_userProfile_infoTabs_TB_featured">
                        {
                            videos?.map((video, i) => (
                                video.isFeatured &&
                                (
                                    <>
                                    <div key={i} className="streaming_userProfile_infoTabs_TB_featured_video">
                                        <video src={video.video} 
                                                autoPlay  
                                                controls
                                        />
                                    </div>
                                    <div className="streaming_userProfile_infoTabs_TB_featured_description"
                                        style={{
                                            display: width <= 375 && (isSidebarOpen && "none")
                                        }}
                                    >
                                        <h5>{video.title}</h5>
                                        <small className="description__video_owner">{video.user.name}</small>
                                        <small className="description__video_description">
                                            {featuredDescriptionSubstringCounter(video.description)}
                                        </small>
                                    </div>
                                    </>
                                )
                            ))
                        }
                        {
                            videos?.length === 0 &&
                            (
                                <NavLink to="/settings" className="streaming_userProfile_inforTabs_featured_placeholder">
                                    <div className="featured_empty_placeholder">
                                        <i className="fa-solid fa-video-slash"></i>
                                        <p>No Featured Video</p>
                                    </div>
                                </NavLink>
                            )
                        }
                        {
                            !haveFeatured &&
                            (
                                <NavLink to="/settings" className="streaming_userProfile_inforTabs_featured_placeholder">
                                    <div className="featured_empty_placeholder">
                                        <i className="fa-solid fa-video-slash"></i>
                                        <p>No Featured Video</p>
                                    </div>
                                </NavLink> 
                            )
                        }
                    </div>
                    <hr />
                    <div className="streaming_userProfile_infoTabs_TB_featured_user_uploads">
                        {
                            activeTab === "Profile" && (
                                <>
                                <h5 className="sectionTitle">{user.firstName} {user.lastName} Uploaded Videos</h5>
                                <div className="streaming_userProfile_infoTabs_Tb_featured_USfeatured">
                                {
                                    videos?.map((video,i) => (
                                        video.user._id === user._id && (
                                            <MusicCard key={i} index={i} videos={videos} video={video} isHovered={isHovered} setVideos={setVideos} setIsHovered={setIsHovered} titleSubstringCounter={titleSubstringCounter} />
                                        )
                                    ))
                                }
                                </div>
                                </>
                            )
                        }
                        {
                            activeTab === "History" && (
                                <>
                                <h5 className="sectionTitle">{activeTab}</h5>
                                <div className="streaming_userProfile_infoTabs_Tb_featured_USfeatured">
                                {
                                    usersHistory?.map((history,i) => (
                                        <MusicCard key={i} index={i} videos={usersHistory} video={history} isHovered={isHovered} setVideos={setUsersHistories} setIsHovered={setIsHovered} titleSubstringCounter={titleSubstringCounter} />
                                    ))
                                }
                                </div>
                                </>
                            )
                        }
                        {
                            activeTab === "Playlist" && (
                                <>
                                <h5 className="sectionTitle">{activeTab}</h5>
                                <div className="streaming_userProfile_infoTabs_Tb_featured_USfeatured">
                                    <div onClick={() => setRenderPlaylistForm(prev => !prev)} className="streaming_userProfile_infoTabs_Tb_featured_playlist_uploader">
                                        <div className="playlist_uploader_icons">
                                            <i className="fa-solid fa-circle-plus"></i>
                                            <p>ADD PLAYLIST</p>
                                        </div>
                                    </div>
                                    {
                                        !renderPlaylistForm ?
                                        user?.playlists?.map((playlist,i) => (
                                            <PlaylistCard key={i} playlist={playlist} user={user} setUser={setUser} setVideos={setVideos} titleSubstringCounter={titleSubstringCounter} videos={videos} />
                                        ))
                                        :
                                        (
                                            <div className="playlist_form">
                                                <div className="playlist_img_container">
                                                    <img 
                                                        src={ playlistImgRenderer || "https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/e4bc7023-72be-4875-b1e7-b23417b03812/dcrhs6h-bda1336a-da3c-4473-a390-9d38716ecdd7.png/v1/fill/w_1024,h_1442,q_80,strp/synyster_gates_fanart_by_nanazsuzsi_dcrhs6h-fullview.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7ImhlaWdodCI6Ijw9MTQ0MiIsInBhdGgiOiJcL2ZcL2U0YmM3MDIzLTcyYmUtNDg3NS1iMWU3LWIyMzQxN2IwMzgxMlwvZGNyaHM2aC1iZGExMzM2YS1kYTNjLTQ0NzMtYTM5MC05ZDM4NzE2ZWNkZDcucG5nIiwid2lkdGgiOiI8PTEwMjQifV1dLCJhdWQiOlsidXJuOnNlcnZpY2U6aW1hZ2Uub3BlcmF0aW9ucyJdfQ.H-HUXnjFUc4ZYKzqV34j5XyfNucyEIfRmqsmTceKvMI"} 
                                                        alt="playlist_img_uploader"
                                                    />
                                                    <input  
                                                        name="playlist_img"
                                                        type="file"
                                                        onChange={onHandlePlaylistImgRenderer}
                                                    />
                                                </div>
                                                <div className="playlist_form_inputs_container">
                                                    {
                                                        errMsg !== "" &&
                                                        (
                                                            <div className="playlist_form_errMsg">
                                                                <small>{errMsg}</small>
                                                            </div>
                                                        )
                                                    }
                                                    <div className="playlist_form_inputsContainer_mainContent">
                                                        <label htmlFor="playlist_title">TITLE</label>
                                                        <input 
                                                            type="text"
                                                            name="name"
                                                            onChange={(e) => setPlaylistTitle(e.target.value)}
                                                            placeholder="Hardwired to Self-Destruct"
                                                        />
                                                        <input 
                                                            type="text"
                                                            name="year"
                                                            onChange={(e) => setYear(e.target.value)}
                                                            placeholder="2016"
                                                        />
                                                        <div className="playlistBtn">
                                                            <button onClick={() => navigate(-1)}>CANCEL</button>
                                                            <button onClick={onhandleCreatePlaylist}>CREATE</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        )
                                    }
                                </div>
                                </>
                            )
                        }
                        {
                            activeTab === "Subscribed Channels" && (
                                <>
                                <h5 className="sectionTitle">{activeTab}</h5>
                                <div className="streaming_userProfile_infoTabs_Tb_featured_USfeatured">
                                {
                                    user.subscriptions?.map((subscriber,i) => (
                                        <SubscribedChannel key={i} user={user._id} subscriber={subscriber} setUser={setUser} axiosPrivate={axiosPrivate} setVideos={setVideos} usersInfo={user} />
                                    ))
                                }
                                </div>
                                </>
                            )
                        }
                    </div>
                </div>
            </div>
        </div>
    </div>
    )
}

export default UserProfile