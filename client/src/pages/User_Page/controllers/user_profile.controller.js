const user_profile_controller = () => {
    const titleSubstringCounter = (title) => {
        return title.length > 36 ? title.substring(0,30) + "..." : title; 
    }

    const featuredDescriptionSubstringCounter = (description) => {
        return description.length > 350 ? description.substring(0,350) + "..." : description; 
    }

    const pageAnalysis = (location) => {
        return location === "/watch" ? true : false;
    }   

    return {
        titleSubstringCounter,
        pageAnalysis,
        featuredDescriptionSubstringCounter
    }
}

export default user_profile_controller