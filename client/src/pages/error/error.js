import React from 'react'
import { useNavigate } from 'react-router-dom';
import "./error.css";
const Error = () => {
  const navigate = useNavigate();
  const backToHome = () => {
    navigate("/");
  }
  return (
    <div className="streaming__error_messages">
      <div className="not_found">
        <div className="notfound-404">
          <h1>404</h1>
          <h2>Page not found</h2>
        </div>
        <div className="streaming_error_homeBtn">
          <button onClick={backToHome}>BACK TO HOMEPAGE</button>
        </div>
      </div>
    </div>
  )
}

export default Error