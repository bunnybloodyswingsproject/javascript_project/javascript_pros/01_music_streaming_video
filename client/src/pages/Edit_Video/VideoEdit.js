import React, { useEffect, useRef, useState } from 'react'
import { NavLink, useLocation } from 'react-router-dom'
import EditForm from '../../components/EditForm';
import "./editvideo.css";
import useAuth from '../../hooks/useAuth';
import EditVideoControllers from "./controllers/EditVideoControllers"
import useLayout from '../../hooks/useLayout';
import useWindowDimensions from '../../hooks/useWindowDimensions';
import usePhoneSidebarOpen from '../../hooks/usePhoneSidebarOpen';

const VideoEdit = () => {
  const {
    pageAnalysis
  } = EditVideoControllers();
  const location = useLocation().pathname;
  const { setIsWatchPage } = useLayout();
  const [currentTab, setCurrentTab] = useState("Edit Video");
  const {user, setUser} = useAuth();
  const [updatingVideo, setUpdatingVideo] = useState(useLocation()?.state?.video)
  
  useEffect(() => {
    let isMounted = false;
    !isMounted && setIsWatchPage(pageAnalysis(location));

    return () => isMounted = true;
  }, [pageAnalysis, setIsWatchPage, location])

  const { width } = useWindowDimensions();
  const { setIsPhoneSidebarOpen } = usePhoneSidebarOpen()

  const closeNavbarRef = useRef(false);
  useEffect(() => {
      closeNavbarRef.current && location === "/settings" && setIsPhoneSidebarOpen(false);
      return () => closeNavbarRef.current = true;
  }, [location,  setIsPhoneSidebarOpen]);

  return (
    <div className="streaming__subpages">
      {
        width <= 376 &&
        (
          <div className="streaming__watch_navbar">
            <NavLink to="/" className="streaming__sidebar_brandname">
                <div className="streaming__sidebar_logo"></div>
                <h3 className="streaming__sidebar_logo_text">Batugs</h3>
            </NavLink>
            <div className="streaming__watch_search_engine_formInputs">
                <div className="streaming__watch_user_avatar">
                    <img 
                        src={user.image}
                        alt="user_avatar"
                        onError={(e) => {
                            e.target.attributes.src.value = "https://www.pngitem.com/pimgs/m/150-1503945_transparent-user-png-default-user-image-png-png.png"
                        }}
                    />
                </div>
            </div>
          </div>
        )
      }
        <div className="streaming_settings_uploader_container">
            <div className="streaming_settings_uploader_pageDiv_title" >
              <div className="streaming_settings_uploader_currentLink">
                <div className='streaming_settings_uploader_title'>
                  <h4>{currentTab}</h4>
                </div>
              </div>
            </div>
            <div className="streaming__settings_uploader_content_container">
              <EditForm 
                user={user} 
                setUser={setUser} 
                updatingVideo={updatingVideo} 
                setUpdatingVideo={setUpdatingVideo} 
              />
            </div>
        </div>
    </div>
  )
}

export default VideoEdit