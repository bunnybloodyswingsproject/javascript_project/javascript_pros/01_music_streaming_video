import useAuth from "../../../hooks/useAuth";
import useAxiosPrivate from "../../../hooks/useAxiosPrivate";
import useVideo from "../../../hooks/useVideo";

const WatchController = () => {
    const axiosPrivate = useAxiosPrivate();
    const { setUser } = useAuth();
    const { setVideos } = useVideo();

    const descriptionSubstringCounter = (content) => {
        return content.length > 203 ? content.substring(0,200) + "..." : content; 
    }
    
    const relatedVideoTitle = (content) => {
        return content.length > 45 ? content.substring(0,45) + "..." : content; 
    }

    const pageAnalysis = (location) => {
        return location === "/watch" ? true : false;
    }
    
    function intToString (value) {
        var newValue = value;
        if (value >= 1000) {
            var suffixes = ["", "K", "M", "B","T"];
            var suffixNum = Math.floor( (""+value).length/3 );
            var shortValue = '';
            for (var precision = 2; precision >= 1; precision--) {
                shortValue = parseFloat( (suffixNum != 0 ? (value / Math.pow(1000,suffixNum) ) : value).toPrecision(precision));
                var dotLessShortValue = (shortValue + '').replace(/[^a-zA-Z 0-9]+/g,'');
                if (dotLessShortValue.length <= 2) { break; }
            }
            if (shortValue % 1 != 0)  shortValue = shortValue.toFixed(1);
            newValue = shortValue+suffixes[suffixNum];
        }
        return newValue;
    }

    const dateConverter = (date) => {
        const createdDate = Math.floor(new Date(date).getTime() / 1000)
        const dateToday = Math.floor(new Date().getTime() / 1000);
        const difference = dateToday - createdDate;
        let output = ``;
        if (difference < 60) {
            // Less than a minute has passed:
            output = `${difference} seconds ago`;
        } else if (difference < 3600) {
            // Less than an hour has passed:
            output = `${Math.floor(difference / 60)} minutes ago`;
        } else if (difference < 86400) {
            // Less than a day has passed:
            output = `${Math.floor(difference / 3600)} hours ago`;
        } else if (difference < 2620800) {
            // Less than a month has passed:
            output = `${Math.floor(difference / 86400)} days ago`;
        } else if (difference < 31449600) {
            // Less than a year has passed:
            output = `${Math.floor(difference / 2620800)} months ago`;
        } else {
            // More than a year has passed:
            output = `${Math.floor(difference / 31449600)} years ago`;
        }
        
        return output    
    
    }
   
    function numberWithCommas(x) {
        return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
    }
    
    const getDate = (date) => {
        const Months = [
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"
        ];
        const months = Months[new Date(date).getMonth()];
        const day = new Date(date).getDate();
        const year = new Date(date).getFullYear();

        return `${months} ${day}, ${year}`
    }

    const getRelatedVideos = async (controller, video) => {
        
        try {
            const response = await axiosPrivate.get("videos/related_movies",
                {
                    params: {
                    _id: video._id,
                    userId: video.user._id,
                    category: video.category,
                    title: video.title
                },
            },
            {
                signal: controller.signal
            },{
                headers: { "Content-Type": "application/json" },
                withCredentials: true
            }
            )

            if(response.data.success) {
                return response.data.relatedVideos
            }else{
                return response.data.relatedVideos
            }
        }catch(error) {
            return console.log(error);
        }
    }

    const handleVideoHistory = async(e, user, video) => {
        const watchedDuration = Math.floor(e.target.currentTime);
        const userId = user._id;
        const videoId = video._id;

        try {
            const response = await axiosPrivate.post("videos/save_history",
                JSON.stringify({
                    _id: videoId,
                    userId: userId,
                    watchedTime: watchedDuration
                }), {
                    headers: { "Content-Type": "application/json" },
                    withCredentials: true
                }
            );

            if(response.data.success) {
                if(response.data.new) {
                    setUser(prev => {
                        return {
                            ...prev,
                            history: [
                                ...prev.history, 
                                {
                                    _id: response.data.newId,
                                    videoId: video._id,
                                    title: video.title,
                                    watched: watchedDuration,
                                    thumbnail: video.thumbnail,
                                    hours: video.hours,
                                    minutes: video.minutes,
                                    seconds: video.seconds,
                                }
                            ]
                        }
                    })
                }else{
                    setUser(prev => {
                        return {
                            ...prev,
                            history: [
                                ...prev.history, 
                                {
                                    watched: prev.watched = watchedDuration,
                                }
                            ]
                        }
                    })
                }
            }
        }catch(error) {
            console.log(error);
        }
        
    }

    return {
        pageAnalysis,
        descriptionSubstringCounter,
        intToString,
        numberWithCommas,
        dateConverter,
        getDate,
        getRelatedVideos,
        relatedVideoTitle,
        handleVideoHistory
    }
}

export default WatchController