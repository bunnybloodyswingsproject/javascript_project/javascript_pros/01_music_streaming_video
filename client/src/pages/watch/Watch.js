import "./watch.css";
import useLayout from "../../hooks/useLayout";
import WatchController from "./controllers/WatchController";
import { useEffect, useMemo, useRef, useState } from "react";
import { Link, NavLink, useLocation, useNavigate } from "react-router-dom";
import useAuth from "../../hooks/useAuth";
import useAxiosPrivate from "../../hooks/useAxiosPrivate";
import UsersAuth from "../../hooks/usersAuth";
import useVideo from "../../hooks/useVideo";
import useWindowDimensions from "../../hooks/useWindowDimensions";

const Watch = () => {
    const {
        pageAnalysis,
        descriptionSubstringCounter,
        intToString,
        numberWithCommas,
        dateConverter,
        getDate,
        getRelatedVideos,
        relatedVideoTitle,
        handleVideoHistory
    } = WatchController();
    const { videos } = useVideo();
    const {setIsWatchPage} = useLayout();
    const location = useLocation().pathname;
    const [video, setVideo] = useState(useLocation().state.video);
    const { users ,setUsers } = UsersAuth();
    const [comment, setComment] = useState("");
    const { user, setUser } = useAuth();
    const [openDescription, setOpenDescription] = useState(false);
    const axiosPrivate = useAxiosPrivate();
    const likesBtn = useRef();
    const dislikesBtn = useRef();
    const navigate = useNavigate();
    const [isReplyOpen, setisReplyOpen] = useState(false);
    const [reply, setReply] = useState("");
    const [replyInputOpen, setReplyInputOpen] = useState(false);
    const replyRef = useRef();
    const [relatedVideos, setRelatedVideos] = useState([]);
    const effectRan = useRef(false);
    const videoPlayer = useRef();
    const playlist = useLocation()?.state?.playlist;
    const usersEffectRan = useRef(false);
    const [userChannel, setUserChannel] = useState({});
    const [alreadySubscriber, setAlreadySubscriber] = useState(false);
    const [channelVideos, setChannelVideo] = useState([])
    useEffect(() => {
        const controller = new AbortController();
        const getUsers = async() => {
            try {
                const response = await axiosPrivate.get("user/getusers", {
                    headers: { "Content-Type": "application/json" },
                    withCredentials: true
                },{
                    signal: controller.signal
                });

                if(!response.data.success) {
                    return alert(response.data.message);
                }

                setUsers(response.data.user);
            }catch(error) {
                return console.log(error);
            }
        }

        usersEffectRan.current && getUsers();

        return () => {
            usersEffectRan.current = true;
            controller.abort();
        }
    }, [axiosPrivate, setUsers]);
    
    useEffect(() => {
        let isMounted = false;
        !isMounted && setIsWatchPage(pageAnalysis(location));

        return () => isMounted = true;
    }, [setIsWatchPage, pageAnalysis, location]);

    useEffect(() => {
        video == null && navigate("/");
    }, [video, navigate]);

    useEffect(() => {
        let isMounted = true;
        const controller = new AbortController();

        if(effectRan.current) {
            getRelatedVideos(controller, video).then(function (data) {
                setRelatedVideos(data)
            })
        }

        return () => {
            isMounted = false;
            controller.abort();
            effectRan.current = true;
        }
    }, [video._id, video.category,video.user._id, setRelatedVideos]);

    const onHandleLikes = async (e) => {
        try{
            const response = await axiosPrivate.post("videos/likes", 
                JSON.stringify({
                    _id: video._id,
                    userId: user._id
                }), {
                    headers: { "Content-Type": "application/json" },
                    withCredentials: true
                }
            )

            if(response.data.success) {
                if(response.data.dislikersData != null && response.data.dislikersData.dislikers.length > 0 ) {

                    let dislikersList = response.data.dislikersData.dislikers;
                    let userId = user._id;
                    setVideo(prev => {
                        return {
                            ...prev,
                            dislikers: dislikersList.filter(disliker => disliker._id !== userId)
                        }
                    })
                }

                if(response.data.code === "REPLICA_LIKERS_ID") {
                    return alert("You already liked this video");
                }

                setVideo(prev => {
                    return {
                        ...prev,
                        likers: [...prev.likers, user._id]
                    }
                });
            }
        }catch(error) {
            console.log(error);
        }
    }
    
    const onHandleDislikes = async (e) => {
        try{
            const response = await axiosPrivate.post("videos/dislikes", 
                JSON.stringify({
                    _id: video._id,
                    userId: user._id
                }), {
                    headers: { "Content-Type": "application/json" },
                    withCredentials: true
                }
            )

            if(response.data.success) {
                if(response.data.likersData != null && response.data.likersData.likers.length > 0 ) {
                    let likersList = response.data.likersData.likers;
                    let userId = user._id;
                    setVideo(prev => {
                        return {
                            ...prev,
                            likers: likersList.filter(liker => liker._id !== userId)
                        }
                    })
                }

                if(response.data.code === "REPLICA_DISLIKERS_ID") {
                    return alert("You already disliked this this video");
                }

                setVideo(prev => {
                    return {
                        ...prev,
                        dislikers: [...prev.dislikers, user._id]
                    }
                })
            }
        }catch(error) {
            console.log(error);
        }
    }

    const handleComments = async(e) => {
        e.preventDefault();
        try {
            const response = await axiosPrivate.post("videos/add_comment", 
                JSON.stringify({
                    _id: video._id,
                    userId: user._id,
                    comment: comment,
                }), {
                    headers: { "Content-type": "application/json" },
                    withCredentials: true
                }
            )

            if(response.data.success) {
                setVideo(prev => {
                    return {
                        ...prev,
                        comments: [...prev.comments, {
                            _id: response.data.commentId,
                            user: {
                                _id: user._id,
                                name: user.firstName + " " + user.lastName,
                                image: user.image
                            },
                            comment: comment,
                            createdAt: new Date().getTime(),
                            replies: []
                        }]
                    }
                })
            }
            setComment("");
        }catch(error) {
            console.log(error);
        }
    }

    const handleReplyBtn = (e) => {
        setisReplyOpen(prev => !prev)
    }

    const handleReply = async(e) => {
        // e.preventDefault();
        if(reply === "") {
            alert("Reply should not be blank");
        }

        try {
            const response = await axiosPrivate.post("videos/reply",
                JSON.stringify({
                    _id: e.target.id,
                    reply: reply,
                    userId: user._id
                }), {
                    headers: { "Content-Type": "application/json" },
                    withCredentials: true
                }
            );

            if(response.data.success) {



                // setVideo(prev => {
                //     const updateReply = video.comments.map(comment => {
                //             if(comment._id === e.target.id) {
                //                 return [comment,
                //                     comment.replies.push(
                //                         ...comment.replies, 
                //                     {
                //                         _id: response.data.replyId,
                //                         user: {
                //                             _id: user._id,
                //                             name: user.firstName + " " + user.lastName,
                //                             image: user.image
                //                         },
                //                         reply: reply,
                //                         createdAt: new Date().getTime(),
                //                     }
                //                     )   
                //                 ]
                //             }else{
                //                 return comment
                //             }
                //         })
                    

                //     return {
                //         ...prev,
                //         comments: updateReply
                //     }
                // })

                navigate('/')
            }
        }catch(error) {
            console.log(error);
        }
    }

    const onhandleSubscribe = async(e) => {
        try {
            const response = await axiosPrivate.post("videos/subscribe", 
                JSON.stringify({
                    userId: user._id,
                    videoId: video._id,
                    videoOwner: video.user._id
                }),
                {
                    headers: { "Content-Type": "application/json" },
                    withCredentials: true
                }
            );
            
            if(!response.data.success) {
                return alert(response.data.message);
            }

            if(response.data.code === 409) {
                return alert(response.data.message);
            }else if(response.data.code === 209){
                return alert(response.data.message)
            }

            users.map(videoOwner => {
                if(videoOwner._id !== video.user._id) {
                    return videoOwner;
                }else{
                    return {
                        ...videoOwner,
                        subscriptions: [
                            ...videoOwner.subscriptions,
                            {
                                _id: user._id,
                                name: user.firstName + " " + user.lastName,
                                subscribers: user.subscriptions.length,
                                thumbnail: user.thumbnail
                            }
                        ]
                    }
                }
            });
            window.location.reload(true);
        }catch(error) {
            console.log(error);
        }
    }

    const channelVideoEffect = useRef(false);
    useEffect(() => {
        let videoContainer = [];
        channelVideoEffect.current &&
            videos.map(isChannelVideo => {
                isChannelVideo.user._id === video.user._id &&
                    videoContainer.push(isChannelVideo);
                    return isChannelVideo
            });

            setChannelVideo(videoContainer);
        return () => channelVideoEffect.current = true;
    }, [video.user._id, videos]);

    let subscriberAnalysis = useRef(false);
    useEffect(() => {
        subscriberAnalysis.current &&
        users.map(isSubscriber => {
            if(isSubscriber._id === video.user._id) {
                setUserChannel(isSubscriber);
                isSubscriber.subscriptions.map(subscriber => {
                    subscriber._id === user._id &&
                        setAlreadySubscriber(true);
                    return subscriber
                })
            }
            return isSubscriber
        })

        return () => subscriberAnalysis.current = true
    }, [users, user._id, video.user._id]);
    
    const [isShowComments, setIsShowComments] = useState(false)
    const onhandleShowComments = () => {
        setIsShowComments(prev => !prev)
    }
    const { width } = useWindowDimensions();

    return (
        <>
            <div className="streaming__watch_navbar">
                <NavLink to="/" className="streaming__sidebar_brandname">
                    <div className="streaming__sidebar_logo"></div>
                    <h3 className="streaming__sidebar_logo_text">Batugs</h3>
                </NavLink>
                <div className="streaming__watch_search_engine_formInputs">
                    <div className="streaming__watch_user_avatar">
                        <img 
                            src={user.image}
                            alt="user_avatar"
                            onError={(e) => {
                                e.target.attributes.src.value = "https://www.pngitem.com/pimgs/m/150-1503945_transparent-user-png-default-user-image-png-png.png"
                            }}
                        />
                    </div>
                </div>
            </div>
            <div className="streaming__watch_content_container">
                <div className="streaming_watch_content_left">
                    <div className="streaming__watch_content_left_video_container">
                        <video 
                            src={video.video} 
                            autoPlay 
                            controls 
                            onPlay={(e) => handleVideoHistory(e, user, video)}
                            onPause={(e) => handleVideoHistory(e, user, video)} 
                            ref={videoPlayer} 
                        />
                        <div className="streaming_watch_left_vc_videoInfo">
                            <div className="streaming_watch_left_vc_videoInfo_titleInfo">
                                <h4>{video.title}</h4>
                                <div className="streaming_watch_left_vc_videoInfo_titleInfo_callToActionButtons">
                                    <Link 
                                        className="streaming_watch_left_artistInfo"
                                        to="/user_profile"
                                        state={{
                                            user: userChannel,
                                            videos: channelVideos
                                        }}
                                    >
                                        <img src={video.user.image} alt="streaming_left_artist_img" />
                                        <Link className="streaming_watch_left_artist_name_and_subscriptions"
                                              to="/user_profile"
                                              state={{
                                                user: userChannel,
                                                videos: channelVideos
                                              }}
                                        >
                                            <h5>{video.user.name}</h5>
                                            <small>{video.user.subscribers} subscriptions</small>
                                        </Link>
                                    </Link>
                                    <div className="streaming_watch_left_artist_likes_and_dislikes">
                                        <div className={alreadySubscriber ? "streaming_watch_left_artist_subscribeBtn_alreadySubscribe" : "streaming_watch_left_artist_subscribeBtn"}>
                                            <button onClick={onhandleSubscribe}>{alreadySubscriber ? "Already Subscribe" : "Subscribe"}</button>
                                        </div>
                                        <button onClick={onHandleLikes} ref={likesBtn} className="likesBtn" value={video.likers.length}><i className="fa-solid fa-thumbs-up"></i> {video.likers.length}</button>
                                        <button onClick={onHandleDislikes} ref={dislikesBtn} className="dislikesBtn" value={video.dislikers.length}>{video.dislikers.length} <i className="fa-solid fa-thumbs-down"></i></button>
                                    </div>
                                </div>
                            </div>
                            <div className="streaming_watch_left_vc_description" onClick={() => setOpenDescription(prev=> !prev)}>
                                <div className="description__views">{
                                    openDescription
                                    ? (<span>{numberWithCommas(video.views)} views</span>)
                                    : (<span>{intToString(video.views)} views</span>)
                                } {
                                    openDescription
                                    ? getDate(video.createdAt)
                                    : (dateConverter(video.createdAt))
                                  }
                                </div>
                                <div className="description__content">
                                    <p>
                                        {
                                           openDescription
                                           ? video.description
                                           : descriptionSubstringCounter(video.description)
                                        }
                                    </p>
                                </div>
                                <div className={openDescription ? "description_icon_open" : "description_icon"}>
                                    <p>Show More</p>
                                </div>
                            </div>
                        </div>
                        <div className="streaming_watch_left_vc_comment_Section">
                            <div className="streaming_watch_left_vc_commentTitle">
                                <span>{video.comments.length}</span><p>Comments</p>
                            </div>
                            <div className="streaming_watch_left_vc_commentContents">
                                <form className="streamimg_watch_left_userOwner_comments" onSubmit={handleComments}>
                                    <Link
                                        to="/user_profile"
                                        state={{
                                            user: user,
                                            videos: videos
                                        }}
                                    >
                                        <img src={user.image} alt="streaming_left_artist_img" />
                                    </Link>
                                    <input 
                                        name="comments"
                                        onChange={(e) => setComment(e.target.value)}
                                        className="owner_comment"
                                        placeholder="Place your comments here..."
                                    />
                                </form>
                                <div className="streaming_watch_showComments">
                                    <div className="showComments" onClick={onhandleShowComments}>
                                        <p>show comments</p>
                                        <i className="fa-solid fa-caret-down"></i>
                                    </div>
                                </div>
                                {
                                    isShowComments && video?.comments?.map((comment, i) => (
                                        <div key={i} className="streaming_watch_left_users_comment">
                                            <Link to="/">
                                                <img src={comment.user.image} alt="streaming_left_artist_img" />
                                            </Link>
                                            <div className="streaming_watch_left_usersComment_init">
                                                <Link className="users_info" to="/">
                                                    <small>{comment.user.name}</small>
                                                    <small>{(dateConverter(comment.createdAt))}</small>
                                                </Link>
                                                <p className="users_comment">{comment.comment}</p>
                                                <div className="streaming_watch_left_usersReply">
                                                    {
                                                        comment.replies.length > 0 &&
                                                        (
                                                            <div className="usersReply_commentsCounts"
                                                                onClick={handleReplyBtn}
                                                                id={comment._id}
                                                            >
                                                                <i className="fa-solid fa-caret-down" onClick={handleReplyBtn} id={comment._id}></i> <span id={comment._id}>{comment.replies.length} replies</span>
                                                            </div>
                                                        )
                                                    }
                                                    {
                                                        !isReplyOpen &&
                                                        (
                                                            <div className="streaming_watch_left_reply_input">
                                                                {
                                                                    replyInputOpen ?
                                                                    (
                                                                        <>
                                                                        <img src={video.user.image} alt="streaming_left_artist_img" />
                                                                        <input 
                                                                            name="reply"
                                                                            onChange={(e) => setReply(e.target.value)}
                                                                            placeholder="Enter your comment here..."
                                                                            ref={replyRef}
                                                                        />
                                                                        <div className="streaming_left_artist_reply_icons">
                                                                            {
                                                                                reply.length > 0 &&
                                                                                (
                                                                                    <i className="fa-brands fa-telegram" onClick={handleReply} id={comment._id}></i>
                                                                                )
                                                                            }
                                                                            <i className="fa-regular fa-rectangle-xmark"
                                                                            onClick={() => {
                                                                                setReplyInputOpen(false)
                                                                                replyRef.current.value=""
                                                                            }}
                                                                            ></i>
                                                                        </div>
                                                                        </>
                                                                    )
                                                                    :
                                                                    (
                                                                        <div className="streaming_watch_left_reply_logo"
                                                                             onClick={() => setReplyInputOpen(prev => !prev) }
                                                                        >
                                                                            <i className="fa-solid fa-comments"></i>
                                                                            <p>Reply</p>
                                                                        </div>
                                                                    )
                                                                }
                                                            </div>
                                                        )
                                                    }
                                                    <div className={isReplyOpen ? "RepliesOpen" : "streaming_watch_left_usersReply_contents"}>
                                                        {
                                                            comment.replies.map((replies, i) => (
                                                                <div key={i} className="users_commentsInfo">
                                                                    <Link to="/">
                                                                        <img src={replies.user.image} alt="streaming_left_artist_img" />
                                                                    </Link>
                                                                    <div className="streaming_watch_left_usersReply_init">
                                                                        <Link to="/" className="users_info">
                                                                            <small>{replies.user.name}</small>
                                                                            <small>{(dateConverter(replies.createdAt))}</small>
                                                                        </Link>
                                                                        <p className="users_comment">{replies.reply}</p>
                                                                    </div>
                                                                </div>
                                                            ))
                                                        }
                                                        <div className="streaming_watch_left_reply_input">
                                                        {
                                                                    replyInputOpen ?
                                                                    (
                                                                        <>
                                                                        <img src={video.user.image} alt="streaming_left_artist_img" />
                                                                        <input 
                                                                            name="reply"
                                                                            onChange={(e) => setReply(e.target.value)}
                                                                            placeholder="Enter your comment here..."
                                                                            ref={replyRef}
                                                                        />
                                                                        <div className="streaming_left_artist_reply_icons">
                                                                            {
                                                                                reply.length > 0 &&
                                                                                (
                                                                                    <i className="fa-brands fa-telegram" onClick={handleReply} id={comment._id}></i>
                                                                                )
                                                                            }
                                                                            <i className="fa-regular fa-rectangle-xmark"
                                                                            onClick={() => {
                                                                                setReplyInputOpen(false)
                                                                                replyRef.current.value=""
                                                                            }}
                                                                            ></i>
                                                                        </div>
                                                                        </>
                                                                    )
                                                                    :
                                                                    (
                                                                        <div className="streaming_watch_left_reply_logo"
                                                                             onClick={() => setReplyInputOpen(prev => !prev) }
                                                                        >
                                                                            <i className="fa-solid fa-comments"></i>
                                                                            <p>Reply</p>
                                                                        </div>
                                                                    )
                                                                }
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    ))
                                }
                            </div>
                        </div>
                    </div>
                </div>
                <div className="streaming_watch_content_Right">
                    <div className="streaming_watch_content_right_video_containers">
                        {
                            playlist?.length > 0 &&
                            (
                                <div className="playlist_container">
                                    <div className="playlist_header">
                                        <h4>Playlist</h4>
                                    </div>
                                    <div className="playlist_video_content">
                                        {
                                            playlist?.map((video, i) => (
                                                <Link 
                                                to="/watch"
                                                state={{ video }}
                                                onClick={() => window.location.reload()}
                                                key={i} className="streaming_watch_content_right_video_cards">
                                                <div className="streaming_watch_content_img">
                                                    <img src={video.thumbnail} alt="subvideos_img_section"/>
                                                </div>
                                                <div className="streaming_watch_right_content_descriptions">
                                                    <h5>{relatedVideoTitle(video.title)}</h5>
                                                    <small>{video.user.name}</small>
                                                    <div className="streaming_watch_right_content_descriptions_date_span">
                                                        <small>{intToString(video.views)} views</small>
                                                        <small>•</small>
                                                        <small>{dateConverter(video.createdAt)}</small>
                                                    </div>
                                                </div>
                                            </Link>
                                            ))
                                        }
                                    </div>
                                </div>
                            )
                        }
                        {
                            relatedVideos.map((relatedVid, i) => (
                                <Link 
                                    to="/watch"
                                    state={{ video }}
                                    onClick={() => window.location.reload()}
                                    key={i} className="streaming_watch_content_right_video_cards">
                                    <div className="streaming_watch_content_img">
                                        <img src={relatedVid.thumbnail} alt="subvideos_img_section"/>
                                    </div>
                                    <div className="streaming_watch_right_content_descriptions">
                                        <h5>{relatedVideoTitle(relatedVid.title)}</h5>
                                        <small>{relatedVid.user.name}</small>
                                        <div className="streaming_watch_right_content_descriptions_date_span">
                                            <small>{intToString(relatedVid.views)} views</small>
                                            {width => 376 && <small>•</small>}
                                            <small>{dateConverter(relatedVid.createdAt)}</small>
                                        </div>
                                    </div>
                                </Link>
                            ))
                        }
                        
                    </div>
                </div>
            </div>
        </>
    )
}

export default Watch