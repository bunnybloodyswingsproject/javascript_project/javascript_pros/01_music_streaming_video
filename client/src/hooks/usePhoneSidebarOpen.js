import { useContext } from "react";
import { phoneSidebarContext } from "../context/PhoneNavbarSlider";


const usePhoneSidebarOpen = () => {
    return useContext(phoneSidebarContext);
}

export default usePhoneSidebarOpen;