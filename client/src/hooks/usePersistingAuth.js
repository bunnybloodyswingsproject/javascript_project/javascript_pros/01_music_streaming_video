import axios from "axios";
import { useNavigate } from "react-router-dom";
import useAuth from "./useAuth";

const usePersistingAuth = () => {
    const {setUser} = useAuth();
    const navigate = useNavigate();
    const axiosInstance = axios.create({
        baseUrl: process.env.REACT_APP_API_URL
    });

    const persistingAuth = async () => {
        try {
            const response = await axiosInstance.post("/user/persistingauth", {
                withCredentials: true
            });

            if(response.data.user === null) {
                alert("Fetching of data failed. Please login again.");
                navigate("/login");
            }
    
            setUser(prev => {
                return {
                    ...prev,
                    "_id": response.data.user._id,
                    "firstName": response.data.user.firstName,
                    "lastName": response.data.user.lastName,
                    "email": response.data.user.email,
                    "password": response.data.user.password,
                    "coverPhoto": response.data.user.coverPhoto,
                    "image": response.data.user.image,
                    "subscribers": response.data.user.subscribers,
                    "history": response.data.user.history,
                    "subscriptions": response.data.user.subscriptions,
                    "playlists": response.data.user.playlists,
                    "videos": response.data.user.videos,
                    "isAdmin": response.data.user.isAdmin,
                    "notifications": response.data.user.notifications,
                    "createdAt": response.data.user.createdAt,
                    "updatedAt": response.data.user.updatedAt,
                };
            });
    
            return response.data.user
        }catch(error) {
            console.log(error);
            navigate("/")
        }
    }
    return persistingAuth;
}

export default usePersistingAuth;