import { useContext } from "react";
import { SidebarContext } from "../context/SidebarProvided";

const useSidebarOpen = () => {
    return useContext(SidebarContext);
}

export default useSidebarOpen;