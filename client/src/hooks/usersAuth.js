import { useContext } from 'react'
import { UsersContext } from "../context/UsersProvider"
const UsersAuth = () => {
  return useContext(UsersContext);
}

export default UsersAuth