import { useContext } from "react";
import { PageLayoutContext } from "../context/PageLAyoutModifier";

const useLayout = () => {
    return useContext(PageLayoutContext);
}

export default useLayout;