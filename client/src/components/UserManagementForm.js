import { useState, useRef, useEffect } from 'react'
import { useNavigate } from 'react-router-dom';
import useAuth from '../hooks/useAuth';
import useAxiosPrivate from "../hooks/useAxiosPrivate";
import storage from "../firebase";
import {ref, uploadBytesResumable, getDownloadURL} from "firebase/storage";

const UserManagementForm = () => {
  const emailLabel = useRef();
  const passwordLabel = useRef();
  const confirmPasswordLabel = useRef();
  const [email, setEmail] = useState("");
  const { setUser, user } = useAuth();
  const axiosPrivate = useAxiosPrivate();

  const PASS_REGEX = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%]).{5,50}$/;
  
  const [PwdFocus, setPwdFocus] = useState(false);
  const [pwd, setPwd] = useState("");
  const [confirmPwd, setConfirmPwd] = useState("");
  const [validPwd, setValidPwd] = useState(false);
  const [validMatchPwd, setValidMatchPwd] = useState(false);
  const [matchPwdFocus, setMatchPwdFocus] = useState(false);
  const [errMsg, setErrMsg] = useState("");
  const navigate = useNavigate();
  const [userWallpaperRendered, setUserWallpaperRendered] = useState(null);
  useEffect(() => {
    const result = PASS_REGEX.test(pwd);
    setValidPwd(result);
    const match = pwd === confirmPwd;
    setValidMatchPwd(match)
  }, [pwd, confirmPwd]);

  useEffect(() => {
    setErrMsg("");
  }, [pwd, confirmPwd])

  
  const onhandleUpdate = async () => {
    if(!validPwd || !validMatchPwd) {
      return alert("Please provide a proper password.")
    }

    const response = axiosPrivate.post("user/user_management",
      JSON.stringify({
        userId: user._id,
        password: pwd,
        coverPhoto: userWallpaperRendered === null && user.coverPhoto === "" ? "" : userWallpaperRendered !== null && user.coverPhoto === "" ? userWallpaperRendered : user.coverPhoto
      }),
      {
        headers: { "Content-Type": "application/json" },
        withCredentials: true
      }
    );

    if(!response.data.success) {
      return alert(response.data.message);
    }

    window.location.reload(true)
    navigate("/");
  }

  const upload = (items) => {

    items.forEach(item => {
        const filename = new Date().getTime() + item.label + item.file.name;
        const storageRef = ref(storage, "/batugst_file_uploader/" + filename);
        const uploadTask = uploadBytesResumable(storageRef, item.file);
        uploadTask.on("state_changed", (snapshot) => {
            let progress = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
            console.log(`Upload is ${progress}% done`);
            switch(snapshot.state) {
                case "paused":
                    console.log("Upload is pause");
                    break;
                case "running":
                    console.log("Upload is running");
                    break;
                default:
                    console.log("Uploading the files....");
                    break;
            }
        },
        (err) => {console.log(err)},
        () => {
            getDownloadURL(uploadTask.snapshot.ref).then(url => {
              setUserWallpaperRendered(url);
            })
        })
    })
}

  const onhandleWallpaper = (e) => {
    upload([{
      file: e.target.files[0]
    }])
  }

  console.log(userWallpaperRendered)

  return (
    <div className="user_management_container">
      <h4>Settings</h4>
      <form className="user_management_form" onSubmit={onhandleUpdate}>
        <div className="user_management_basicInfo">
          <div className="form_div">
                <input type="text" 
                        className="form_input" 
                        placeholder=" " 
                        name="email"
                        autoComplete="new-email" 
                        id="email"
                        onChange={(e) => setEmail(e.target.value)}
                        ref={emailLabel}
                        required
                        value={user.email}
                />
                <label htmlFor='email' className='form_lable' onClick={() => emailLabel.current.focus()}>Email</label>
              </div>
              <div className="form_div">
                <input type="password" 
                        id="password"
                        className="form_input" 
                        placeholder=" " 
                        name="password"
                        autoComplete="new-password"  
                        ref={passwordLabel}
                        onChange={(e) => setPwd(e.target.value)}
                        required
                        onFocus={() => setPwdFocus(true)}
                        onBlur={() => setPwdFocus(false)}

                />
                <label htmlFor='password' className='form_lable' onClick={() => passwordLabel.current.focus()}>Password</label>
              </div>
              <p id="pwdnote" className={pwd && !validPwd ? "instructions" : "offscreen"}>
              <i className="fa-solid fa-circle-info"></i>
              5 to 24 characters. <br/>
              Must include uppercase and lowercase letters, a number and a special character <br />
              Allowed special characters: <span aria-label="exclamation mark">!</span>
              <span aria-label="at symbol"> @</span> <span aria-label='hashtag'># </span> 
              <span aria-label='dollar sign'>$</span> <span aria-label='percent'>%</span>
              </p>
              <div className="form_div">
                <input type="password" 
                        id="confirmpassword"
                        className="form_input" 
                        placeholder=" " 
                        name="confirmpassword"
                        autoComplete="confirmpassword"  
                        ref={confirmPasswordLabel}
                        onChange={(e) => setConfirmPwd(e.target.value)}
                        required
                        onFocus={() => setMatchPwdFocus(true)}
                        onBlur={() => setMatchPwdFocus(false)}
                />
                <label htmlFor='password' className='form_lable' onClick={() => passwordLabel.current.focus()}>Password</label>
              </div>
              <p id="confirmnote" className={matchPwdFocus && !validMatchPwd ? "instructions" : "offscreen"}>
              <i className="fa-solid fa-circle-info"></i>
              Password and confirm password is not match.
              </p>
        </div>
        <div className="user_management_featured_update_wallpaper">
          <h4>Wallpaper</h4>
          <div className="user_management_featured_update_wallpaper_img">
            <small><strong>Note: </strong>We advised you to use image with 992 x 550px resolution</small>
            <img src={(userWallpaperRendered === null && user.coverPhoto === "") ? 
                        "https://wallpaperaccess.com/full/1542111.jpg" : 
                          userWallpaperRendered !== null && user.coverPhoto === ""
                            ? userWallpaperRendered
                            : user.coverPhoto
                      } 
                 alt="update_wallpaper" />
            <input 
              type="file"
              name="wallpaper"
              onChange={onhandleWallpaper}
            />
          </div>
        </div>
        <div className="user_management_featured_update">
          <h4>Featured Video</h4>
          <div className="user_management_featured_forms">
            <div className="featured_form_uploader">
              <p>Coming Soon...</p>
              <i className="fa-brands fa-soundcloud"></i>
            </div>
            <div className="featured_textarea">
              <p>Description</p>
              <textarea disabled={true}>This functionality is working in progress. We will announce this once we are done completing the requirements.</textarea>
            </div>
          </div>
        </div>
        <div className="user_management_formBtn">
          <button onClick={() => navigate("/")}>Cancel</button>
          <button>Submit</button>
        </div>
      </form>
    </div>
  )
}

export default UserManagementForm