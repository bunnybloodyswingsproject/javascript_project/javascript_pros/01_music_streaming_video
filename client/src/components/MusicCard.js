import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import useAuth from '../hooks/useAuth'
import useAxiosPrivate from '../hooks/useAxiosPrivate'

const MusicCard = ({titleSubstringCounter, isHovered, setIsHovered, index, video, setVideos, videos}) => {
    let title = video.title
    const [currentIndex, setCurrentIndex] = useState(0);
    const axiosPrivate = useAxiosPrivate();
    const {user, setUser} = useAuth();

    const updateViews = async (e) => {
        try {
            const response = await axiosPrivate.post("videos/update_views",
                JSON.stringify({
                    _id: video._id,
                    userId: video.user._id
                }),
                {
                    headers: { "Content-Type": "application/json" },
                    withCredentials: true
                }
            )
            
            response.then((data) => {
                if(data.data.success) {
                    user.videos.map((usersVideo, i) => {
                        if(usersVideo._id === video._id) {
                            setUser(prev => {
                                return {
                                    ...prev,
                                    videos: [
                                        ...prev.videos,
                                        {views: prev.views + 1}
                                    ]
                                } 
                            })
                        }  
                    });
        
                    setVideos(prev => {
                        return {
                            ...prev,
                            views: prev.views + 1
                        }
                    })
                }
            }).catch(function (error) {
                return error;
            });
        }catch(error) {
            return {
                success: false,
                message: "Something wrong with views update"
            }
        }
    }

    const handleRemoveVideo = async (e) => {
        e.preventDefault();
        try {
            let isVideoOwner = false;

            if(video.user._id === user._id) {
                isVideoOwner = true;
            }

            if(!isVideoOwner) {
                return alert("Unable to process request. Please provide a valid user.")
            }
    
            const response = await axiosPrivate.post("videos/delete_video", 
                JSON.stringify({
                    videoId: video._id,
                    userId: user._id
                })
                ,
                {
                    headers: { "Content-Type": "application/json" },
                    withCredentials: true
                }
            );
    
            if(!response.data.success) {
                return alert(response.data.message);
            }
    
            const filteredVideo = () => {
                user.videos.filter(userVideo => userVideo._id !== video._id)
            }
    
            const filteredHistoryVideo = () => {
                user.history.map(userHistory => userHistory.videoId !== video._id)
            }
    
            const filteredVideos = () => {
                videos.map(isVideo => isVideo._id !== video._id)
            }

            setUser(prev => {
                return {
                    ...prev,
                    videos: filteredVideo
                }
            });

            setUser(prev => {
                return {
                    ...prev,
                    history: filteredHistoryVideo
                }
            });
            
            window.location.reload();
        }catch(error) {
            console.log(error);
        }
    }

    return (
    <Link className="streaming__home_music_card"
        to="/watch"
        state={{ video }}
        onMouseOver={() => {
            setIsHovered(true);
            setCurrentIndex(index);
        }}
        onMouseOut={() => {
            setIsHovered(false);
            setCurrentIndex(index - 1);
        }}
        id={index}
        onClick={updateViews}
    >
        <div className='streaming__home_music_card_images'
                style={{
                transition: ".3s ease"
                }}
        >
            <div className="music_card_overlay"></div>
            {
                isHovered && index === currentIndex?
                <div className="music_card_video">
                    <video src={video.video || "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwcHBwcHBwcHBwcIBw0IBwcHCA8ICQcNFREWFxURExMYHSgiGBolJxMfLTQhMSk6LjQ2Fx8zODMsQygvMywBCgoKBQUFDgUFDisZExkrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrK//AABEIARMAtwMBIgACEQEDEQH/xAAaAAEBAQEBAQEAAAAAAAAAAAAAAQUEAwIH/8QANRABAAECAwMJBAsAAAAAAAAAAAECEQMEBSEx0RQiIyQlNUFRkRIyNEQTNkJDUmFxcoGT0v/EABQBAQAAAAAAAAAAAAAAAAAAAAD/xAAUEQEAAAAAAAAAAAAAAAAAAAAA/9oADAMBAAIRAxEAPwD8NAAAAAAAAFARQAABBQEFQAAAAAAAAAAAFAAAAAAAAAAAABBUAAAAAAAABQAAAAAAUEAAAAAAABBUAAAAAUAAAAAAUAAAABFAQAAAAAEAAABQAAAAAFRQAAAAAAQVAAAAAAAQAFAAAAAAVFAAAAAAARUAAAAAABAAUAAAAABUAUAAAAAEAAAAAAABAAFRQAAAAAAUR2ZvK04OT07MU7JzWHi1VXm83prmn02A5AQAAAAAAAAAAEAAVFAAAAAAAbWpU9g6FX5U41PrjYvBitzUfq9o0bOb7U7PzxcfgDDAAAAAAAAAAABAAAAUAAAAABtahPYunU7dmDg1R/OLm+DFbmoR2Lktvy2WmP7c2DDAAAAAAAAAABAAAAAFQBQAAAG7no7Hy+74LK1T5z0uY4sJuZqYnS6Kb0Xp0vLVTHtReesYn+gYYAAAAAAAAAIAAAAAAACiKAAA28xV1CKb09yYMxF4v8VPFiNfHmeSUxaO4sON9/m43eQMgAAAAAAABAAAAAAAAAAAAVAFaeLMTlotNM20aiKrT7vWYZjvrqvl4j2p7spptE3+/vaQcAAAAAACAAAAAAAAAAAAAAAA7Lx9FTsvPIpjmzu6Te43TE9FT4zyeqI2R+KQcyoAogAAAAAAAAAAAAAAAAAAA949ym+3o6/Dc8HrE8yn9lUA8gAAAAAAAAAAAAAAAAAAAAAHpE82P0qeb7+zHnaQfAAAAAAAAAAAAAAAAAAAAAAD68PV8r4AgAAAAAAAAAAAAAAAAAAAAAC+AAgAAAAAAAAAAAP/2Q=="} 
                        alt="music_card_video" 
                        autoPlay={true} 
                        loop 
                        muted={true} />
                </div>
                :
                <img src={video.thumbnail || "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwcHBwcHBwcHBwcIBw0IBwcHCA8ICQcNFREWFxURExMYHSgiGBolJxMfLTQhMSk6LjQ2Fx8zODMsQygvMywBCgoKBQUFDgUFDisZExkrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrK//AABEIARMAtwMBIgACEQEDEQH/xAAaAAEBAQEBAQEAAAAAAAAAAAAAAQUEAwIH/8QANRABAAECAwMJBAsAAAAAAAAAAAECEQMEBSEx0RQiIyQlNUFRkRIyNEQTNkJDUmFxcoGT0v/EABQBAQAAAAAAAAAAAAAAAAAAAAD/xAAUEQEAAAAAAAAAAAAAAAAAAAAA/9oADAMBAAIRAxEAPwD8NAAAAAAAAFARQAABBQEFQAAAAAAAAAAAFAAAAAAAAAAAABBUAAAAAAAABQAAAAAAUEAAAAAAABBUAAAAAUAAAAAAUAAAABFAQAAAAAEAAABQAAAAAFRQAAAAAAQVAAAAAAAQAFAAAAAAVFAAAAAAARUAAAAAABAAUAAAAABUAUAAAAAEAAAAAAABAAFRQAAAAAAUR2ZvK04OT07MU7JzWHi1VXm83prmn02A5AQAAAAAAAAAAEAAVFAAAAAAAbWpU9g6FX5U41PrjYvBitzUfq9o0bOb7U7PzxcfgDDAAAAAAAAAAABAAAAUAAAAABtahPYunU7dmDg1R/OLm+DFbmoR2Lktvy2WmP7c2DDAAAAAAAAAABAAAAAFQBQAAAG7no7Hy+74LK1T5z0uY4sJuZqYnS6Kb0Xp0vLVTHtReesYn+gYYAAAAAAAAAIAAAAAAACiKAAA28xV1CKb09yYMxF4v8VPFiNfHmeSUxaO4sON9/m43eQMgAAAAAAABAAAAAAAAAAAAVAFaeLMTlotNM20aiKrT7vWYZjvrqvl4j2p7spptE3+/vaQcAAAAAACAAAAAAAAAAAAAAAA7Lx9FTsvPIpjmzu6Te43TE9FT4zyeqI2R+KQcyoAogAAAAAAAAAAAAAAAAAAA949ym+3o6/Dc8HrE8yn9lUA8gAAAAAAAAAAAAAAAAAAAAAHpE82P0qeb7+zHnaQfAAAAAAAAAAAAAAAAAAAAAAD68PV8r4AgAAAAAAAAAAAAAAAAAAAAAC+AAgAAAAAAAAAAAP/2Q=="} 
                        alt="music_card_image"
                        onError={(e) => {
                    e.target.attributes.src.value = "https://apply.sts.net.pk/assets/images/default-upload-image.jpg"
                }} 
            />
            }
        </div>
        <div className='streaming__home_music_card_details'>
            <small className="card__details_music_published">{video.year}</small>
            <p>{titleSubstringCounter(title)}</p>
            <small className="card__details_music_published_artist">{video.user.name}</small>
        </div>
        {
            user._id === video.user._id && (
                <div className="streaming__home_music_card_actionBtn">
                    <Link to="/edit_video" 
                          className="streaming__home_actionBtn_edit stream_editBtn"
                          state={{ video }}
                    >
                        <i className="fa-solid fa-pencil"></i>
                    </Link>
                    <div className="streaming__home_actionBtn_delete stream_deleteBtn" videoId={video._id} userId={user._id} onClick={handleRemoveVideo}>
                        <i className="fa-solid fa-trash" videoId={video._id} userId={user._id} onClick={handleRemoveVideo}></i>
                    </div>
                </div>
            )
        }
    </Link>
    )
}

export default MusicCard