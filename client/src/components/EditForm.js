import React, { useEffect, useRef, useState } from 'react'
import useError from "../hooks/useError";
import storage from "../firebase";
import {ref, uploadBytesResumable, getDownloadURL} from "firebase/storage";
import useAxiosPrivate from '../hooks/useAxiosPrivate';
import { useLocation, useNavigate } from 'react-router-dom';
import useVideo from "../hooks/useVideo";

const EditForm = ({user, setUser, updatingVideo, setUpdatingVideo}) => {
    const titleRef = useRef();
    const descriptionRef = useRef();
    const tagsRef = useRef();
    const yearRef = useRef();
    const navigate = useNavigate();
    const location = useLocation();
    const from = location.state?.from?.pathname || "/";
    const [music, setMusic] = useState(null);
    const [uploader, setUploader] = useState(false);
    const {errMsg, setErrMsg} = useError();
    const options = [
        "Rock",
        "Podcast",
        "Mellow Rock",
        "Pop",
        "Country",
        "Grunge",
        "Post Hardcore",
        "Metal",
        "Folk",
        "Hip Hop",
        "Rap",
        "80s",
        "90s",
        "New songs",
        "Ballad",
    ];
    const booleanOption = ["false", "true"];
    const [video, setVideo] = useState(null);
    const [thumbnail, setThumbnail] = useState(null);
    const [wallpaper, setWallpaper] = useState(null);
    const axiosPrivate = useAxiosPrivate();

    const handleChange = (e) => {
        const value = e.target.value;
        setMusic({...music, [e.target.name]: value});
    }

    const onOptionChangeHandler = (e) => {
        const value = e.target.value;
        setMusic({...music, category: value});
    }

    const onPlaylistChangeHandler = (e) => {
        const value = e.target.value === "Please choose the music genre" ? "" : e.target.value;
        setMusic({...music, playlist: value});
    }

    const onChangeIsFeaturedHandler = (e) => {
        const value = e.target.value === "true" ? true : false;
        setMusic({...music, isfeatured: value});
    }

    const upload = (items) => {
        if(items[0].file === null && items[1].file === null) {
            return setUploader(true);
        } 

        let isOwner = false;
        user.videos.map(usersVideoCollection => {
            if(usersVideoCollection._id === updatingVideo._id) {
                isOwner = true;
            }
        })

        if(isOwner) {
            items.forEach(item => {
                const filename = new Date().getTime() + item.label + item.file.name;
                const storageRef = ref(storage, "/batugst_file_uploader/" + filename);
                const uploadTask = uploadBytesResumable(storageRef, item.file);
                uploadTask.on("state_changed", (snapshot) => {
                    let progress = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
                    console.log(`Upload is ${progress}% done`);
                    switch(snapshot.state) {
                        case "paused":
                            console.log("Upload is pause");
                            break;
                        case "running":
                            console.log("Upload is running");
                            break;
                        default:
                            console.log("Uploading the files....");
                            break;
                    }
                },
                (err) => {console.log(err)},
                () => {
                    getDownloadURL(uploadTask.snapshot.ref).then(url => {
                        setMusic(prev => {
                            return {
                                ...prev,
                                [item.label]: url
                            }
                        });
    
                        setUploader(true);
                    })
                })
            })
        }else{
            console.log("Not the owner.");
            alert("Unable to process request. User is not the owner of the video.")
        }
    }

    const handleUpload = (e) => {
        e.preventDefault();
        if(thumbnail !== null && wallpaper === null) {
            upload([
                {file: thumbnail, label: "thumbnail"}
            ]);
        }else if(thumbnail === null && wallpaper !== null) {
            upload([
                {file: wallpaper, label: "wallpaper"}
            ]);
        }else{
            upload([
                {file: thumbnail, label: "thumbnail"},
                {file: wallpaper, label: "wallpaper"}
            ]);
        }
    }

    const onhandleCreate = async (e) => {
        e.preventDefault();
        let isOwner = false;
        if(typeof music.playlist === "undefined" || music.playlist === null) {
            setMusic({...music, playlist: ""});
        }
        user.videos.map(usersVideoCollection => {
            if(usersVideoCollection._id === updatingVideo._id) {
                isOwner = true;
            }
        })

        if(!isOwner) {
            console.log("Not video owner");
            return alert("Unable to process request. Please provide valid user.")
        }

        try {
            const response = await axiosPrivate.post("videos/update_video", 
                JSON.stringify({
                    videoId: updatingVideo._id,
                    userId: user._id,
                    title: music.title === "" ? updatingVideo.title : music.title,
                    description: music.description === "" ? updatingVideo.description : music.description,
                    tags: music.tags === "" ? updatingVideo.tags : music.tags,
                    category: music.category === "" ? updatingVideo.category : music.category,
                    video: updatingVideo.video,
                    thumbnail: music.thumbnail === "" ? updatingVideo.thumbnail : music.thumbnail,
                    wallpaper: music.wallpaper === "" ? updatingVideo.wallpaper : music.wallpaper,
                    year: music.year === "" ? updatingVideo.year : music.year,
                    playlist: music.playlist === "" && typeof updatingVideo.playlist === "undefined"  ? music.playlist : music.playlist === "" && typeof updatingVideo.playlist !== "undefined" ? updatingVideo.playlist : music.playlist,
                    isfeatured: music.isfeatured === "" ? updatingVideo.isFeatured : music.isfeatured
                }), {
                    headers: { "Content-type": "application/json" },
                    withCredentials: true
            });

            if(!response.data.success) {
                alert(response.data.message);
            }

            if(response.data.success) {
                setUpdatingVideo(prev => {
                    return {
                        ...prev,
                        title: music.title === "" ? updatingVideo.title : music.title,
                        description: music.description === "" ? updatingVideo.description : music.description,
                        tags: music.tags === "" ? updatingVideo.tags : music.tags,
                        category: music.category === "" ? updatingVideo.category : music.category,
                        video: updatingVideo.video,
                        thumbnail: music.thumbnail === "" ? updatingVideo.thumbnail : music.thumbnail,
                        wallpaper: music.wallpaper === "" ? updatingVideo.wallpaper : music.wallpaper,
                        year: music.year === "" ? updatingVideo.year : music.year,
                        playlist: music.playlist === "" ? updatingVideo.playlist : music.playlist,
                        isfeatured: music.isfeatured === "" ? updatingVideo.isfeatured : music.isfeatured
                    }
                });
            }
            navigate("/")
        }catch(error) {
            console.log(error);
            navigate("/")
        }
    }

    useEffect(() => {
        titleRef.current.focus()
    }, []);

    useEffect(() => {
        setErrMsg("")
    }, [music]);
    

    return (
    <form className="streaming__settings_uploaderForms">
        <div className="streaming__settings_uploaderContent_thumbnailUploader">
            <div className="streaming_configs streaming__settings_video_uploader">
                <div className="streaming__settings_video_uploader_icon">
                    <i className="fa fa-solid fa-arrow-up fa-5x"></i>
                    <div className="streaming_settings_video_uploader_icon_oblong"></div>
                </div>
                <div className="streaming__settings_video_uploader_description">
                    <h4>Upload</h4>
                    <p>Upload your video here</p>
                </div>
            </div>
            <div className="streaming_configs streaming__settings_thumbnail_uploader">
                <div className="streaming__settings_video_uploader_icon_secondary">
                    <i className="fa-regular fa-image fa-5x"></i>
                </div>
                <div className="streaming__settings_video_uploader_description">
                    <h4>Thumbnail</h4>
                    <p>Upload your video thumbnail here</p>
                </div>
                <input 
                    type="file"
                    name="thumbnail"
                    onChange={(e) => setThumbnail(e.target.files[0])}
                />
            </div>
            <div className="streaming_configs streaming__settings_wallpaper_uploader">
                <div className="streaming__settings_video_uploader_icon_secondary">
                <i className="fa-regular fa-images fa-5x"></i>
                </div>
                <div className="streaming__settings_video_uploader_description">
                    <h4>Wallpaper</h4>
                    <p>Upload your video wallpaper here</p>
                </div>
                <input 
                    type="file"
                    name="wallpaper"
                    onChange={(e) => setWallpaper(e.target.files[0])}
                />
            </div>
        </div>
        <div className="streaming__settings_uploaderContent_inputforms">
            <div className="streaming__settings_UCI_left">
                <div className="streaming__settings_UCI_left_form_groups">
                    <label>Music Title</label>
                    <input 
                        type="text"
                        placeholder={updatingVideo.title}
                        ref={titleRef}
                        name="title"
                        onChange={handleChange}
                        required={true}
                    />
                </div>
                <div className="streaming__settings_UCI_left_form_groups edit">
                    <label>Description</label>
                    <textarea 
                        placeholder={updatingVideo.description}
                        ref={descriptionRef}
                        name="description"
                        onChange={handleChange}
                        required={true}
                    />
                </div>
                <div className="streaming__settings_UCI_left_form_groups">
                    <label>Playlist</label>
                    <select onChange={onPlaylistChangeHandler}>
                        <option value="">Please choose the music genre</option>
                        {
                            user?.playlists.map((playlist, i) => (
                                <option key={i} selected={updatingVideo.playlist === playlist._id ? true : false} value={playlist._id}>{playlist.title}</option>
                            ))
                        }
                    </select>
                </div>
                <div className="streaming__settings_UCI_left_form_groups">
                    <label>Tags</label>
                    <input 
                        type="text"
                        placeholder={updatingVideo.tags}
                        ref={tagsRef}
                        name="tags"
                        onChange={handleChange}
                        required={true}
                    />
                </div>
            </div>
            <div className="streaming__settings_UCI_right">
                <div className="streaming__settings_UCI_left_form_groups">
                    <label>Category</label>
                    <select onChange={onOptionChangeHandler}>
                        <option>Please choose the music genre</option>
                        {
                            options.map((option, i) => (
                                <option key={i} selected={updatingVideo.category === option ? true : false}>{option}</option>
                            ))
                        }
                    </select>
                </div>
                <div className="streaming__settings_UCI_left_form_groups">
                    <label>Do you want this to be your featured video?</label>
                    <select onChange={onChangeIsFeaturedHandler}>
                        {
                            booleanOption.map((bool, i) => (
                                <option key={i} selected={updatingVideo.isFeatured ? true : false}>{bool}</option>
                            ))
                        }
                    </select>
                </div>
                <div className="streaming__settings_UCI_left_form_groups">
                    <label>Year</label>
                    <input 
                        placeholder={updatingVideo.year}
                        ref={yearRef}
                        name="year"
                        onChange={handleChange}
                        required={true}
                        type="text"
                    />
                </div>
                <div className="streaming__settings_UCI_left_form_groups">
                    <div className="streaming_settings_UCI_right_form_buttons">
                        <button className="form_buttons_cancel" onClick={() => navigate(-1)}>Cancel</button>
                        {
                            uploader  >= 1 
                            ? (<button className="form_buttons_create" onClick={onhandleCreate}>Create</button>)
                            : (<button className="form_buttons_create" onClick={handleUpload}>Upload</button>)
                        }
                        <p className="streaming__settings_right_form_buttons_errMsg">{errMsg}</p>
                    </div>
                </div>
            </div>
        </div>
    </form>
    )
}

export default EditForm