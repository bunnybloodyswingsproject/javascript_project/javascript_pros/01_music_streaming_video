import axios from "axios";
import { NavLink } from "react-router-dom";
import useAxiosPrivate from "../hooks/useAxiosPrivate";
const PlaylistCard = ({titleSubstringCounter, playlist, user, setUser, setVideos, videos}) => {
    const axiosPrivate = useAxiosPrivate();
    const onhandleDeletePlaylist = async (e) => {
        const userId = e.target.attributes.userid.value;
        const playlistId = e.target.attributes.id.value;

        const response = await axiosPrivate.post("videos/delete_playlist",
            JSON.stringify({
                userId: userId,
                playlistId: playlistId
            }),
            {
                headers: { "Content-Type": "application/json" },
                withCredentials: true
            }
        );

        if(!response.data.success) {
            return alert(response.data.message)
        }

        setUser(prev => {
            return {
                ...prev,
                playlist: user.playlists.filter(pl => pl._id !== playlistId)
            }
        });

        let obj;
        videos.map(video => {
            if(video.album === playlistId) {
                return obj = {...video, album: ""}
            }
            
        });
        setVideos(obj);
        window.location.reload(true)
    }

    return (
    <NavLink 
        className="streaming__home_music_card"
        to={playlist.video.length > 0 ? "/watch" : "/"}
        state={{ 
            video: playlist.video[0],
            playlist: playlist.video
        }}
    >
        <div className='streaming__home_music_card_images'
                style={{
                transition: ".3s ease"
                }}
        >
            <div className="music_card_overlay"></div>
            {
                <img src={ playlist.thumbnail || "https://static.wikia.nocookie.net/bfmv/images/2/20/The_Poison_album_art.jpg/revision/latest?cb=20201207091209"} 
                        alt="music_card_image"
                        onError={(e) => {
                    e.target.attributes.src.value = "https://apply.sts.net.pk/assets/images/default-upload-image.jpg"
                }} 
            />
            }
        </div>
        <div className='streaming__home_music_card_details_details'>
            <small className="card__details_music_published">{playlist.year || "Unknown"}</small>
            <p>{titleSubstringCounter(playlist.title)}</p>
            <small>{playlist?.video?.length} videos</small>
        </div>
        <div className="streaming__home_music_card_actionBtn">
            <div className="streaming__home_actionBtn_delete stream_deleteBtn" userid={user._id} id={playlist._id} onClick={onhandleDeletePlaylist}>
                <i className="fa-solid fa-trash" userid={user._id} id={playlist._id} onClick={onhandleDeletePlaylist}></i>
            </div>
        </div>
    </NavLink>
    )
}

export default PlaylistCard