import { useLocation, Navigate, Outlet } from "react-router-dom";
import useAuth from "../hooks/useAuth";
import SideBar from "./SideBar";
import useSidebarOpen from "../hooks/useSidebarOpen";
import useLayout from "../hooks/useLayout";
import useWindowDimensions from "../hooks/useWindowDimensions";
const RequireAuth = () => {
    const {user, setUser} = useAuth();
    const location = useLocation();
    const {isSidebarOpen} = useSidebarOpen();
    const { isWatchPage } = useLayout();
    const { width } = useWindowDimensions();
    return (
        user?.token
             ? isWatchPage 
               ? (
                    <div className="streaming_watch_container">
                        <Outlet />
                    </div>
                 )
               : (
                    <div className={isSidebarOpen ? "streaming__container" : "streaming__container streaming__container_close"}
                        style={{
                            gridTemplateColumns: (width <= 1024 && isSidebarOpen) && "1fr 3.19fr"
                        }}
                    >
                        <SideBar user={user} setUser={setUser}/>
                        <Outlet />
                    </div>
             )
             : <Navigate to="/login" state={{ from: location }} replace />
    )
}

export default RequireAuth;