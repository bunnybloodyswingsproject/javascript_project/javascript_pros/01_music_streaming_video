import { useEffect, useState } from 'react';
import { NavLink, useLocation } from 'react-router-dom';
import useModal from '../hooks/useModal';
import UpdateAvatarModal from './UpdateAvatarModal';
import useSidebarOpen from "../hooks/useSidebarOpen";
import useVideo from '../hooks/useVideo';
import usePhoneSidebarOpen from '../hooks/usePhoneSidebarOpen';
import useWindowDimensions from '../hooks/useWindowDimensions';

const SideBar = ({user, setUser}) => {
  const {isModalOpen, setIsModalOpen} = useModal();
  const {isSidebarOpen, setIsSideOpen} = useSidebarOpen();
  const { videos } = useVideo();
  const { isPhoneSidebarOpen, setIsPhoneSidebarOpen } = usePhoneSidebarOpen();
  const colorOptions = ["FF4A69", "FF8B4E", "FFC44B", "0092F9", "F46500"];
  const { width } = useWindowDimensions();
  const location = useLocation();
  let currentLocation = location?.pathname;
  
  const randomColor = () => {
    return Math.floor(Math.random() * (2-0+1)) + 0;
  }
  const AVATAR_DEFAULT_SVG = `https://avatars.dicebear.com/api/initials/${user.firstName}_${user.lastName}.svg?background=%23${colorOptions[randomColor()]}&radius=50&size=100`;
  let image = user.image;
  useEffect(() => {
    let isMounted = false;
    if(!isMounted) {
      image === "" && setIsModalOpen(true);
    }
    
    return () => {
      isMounted = true
    }
  }, [image, setIsModalOpen]);
  
  const handleSidebar = (e) => {
    setIsSideOpen(prev => !prev);
  }

  const handlePhoneSlider = (e) => {
    setIsPhoneSidebarOpen(prev => !prev)
  }
  
  useEffect(() => {
    const rootElement = document.getElementById("root");
    if(width <= 376 && currentLocation === "/user_profile" && isPhoneSidebarOpen) {
      rootElement.style.overflow = "hidden"
    }else{
      rootElement.style.overflow = "scroll"
    }
  }, [width, isPhoneSidebarOpen, currentLocation])
  return (
    <>
      <div className={width >= 376 ? isSidebarOpen ? "streaming__sidebar" : "streaming__sidebar streaming__sidebar_close" : "streaming__sidebar_phone"}
        style={{
          transform: width <= 376 && (isPhoneSidebarOpen ? "translateX(0)" : "translateX(-240px)"),
          height: width <= 376 && (currentLocation === "/user_profile" && "100vh"),
          position: width <= 376 && (currentLocation === "/user_profile" && "fixed")
        }}
      >
          <div className={isSidebarOpen ? "streaming__sidebar_brand" : "streaming__sidebar_brand_close"}>
              <NavLink to="/" className={isSidebarOpen ? "streaming__sidebar_brandname" : "sidebar__removed_tags"}>
                  <div className="streaming__sidebar_logo"></div>
                  <h3 className="streaming__sidebar_logo_text">Batugs</h3>
              </NavLink>
              {
                width >= 376 ?
                (
                  <div className="streaming__sidebar_navbar_logo" onClick={handleSidebar}>
                  {
                    isSidebarOpen ?
                    (
                      <>
                        <div className="streaming__sidebar_navbar_logo_short"></div>
                        <div className="streaming__sidebar_navbar_logo_long"></div>
                      </>
                    )
                    :
                    <i className="fa-solid fa-xmark"></i>
                  }
                  </div>
                )
                :
                (
                  <div className="streaming__sidebar_navbar_logo" onClick={handlePhoneSlider}>
                    <div className="streaming__sidebar_navbar_logo_short"></div>
                    <div className="streaming__sidebar_navbar_logo_long"></div>
                  </div>
                )
              }
          </div>

          <div className="streaming__dicebear_image">
            <div className={isSidebarOpen ? "dicebear__container" : "dicebear__container_close"} >
            <img src={user.image} onError={(e) => {
              e.target.attributes.src.value = AVATAR_DEFAULT_SVG
            }} alt="streaming_avatar_iamge" />
            </div>
            <h3 className={isModalOpen ? "" : "streaming__avatar_name_display"}>Cyrile Lagumbay</h3>
          </div>

          <div className='streaming__sidebar_menu'>
            <div className="streaming__sidebar_main_menu">
              {
                isSidebarOpen 
                  ? <h4>Menu</h4> 
                  : (
                    <div className="streaming__sidebar_underline_container">
                      <div></div>
                    </div>
                  )
              }
              <div className={isSidebarOpen ? "streaming__sidebar_links" : "streaming__sidebar_links streaming__sidebar_links_close"}>
                <NavLink to="/" end className={({isActive}) => (
                  isActive ? "streaming__logo_navlinks streaming__logo_navlinks_active" : "streaming__logo_navlinks" 
                )}>
                  <i className={isModalOpen ? "fa-solid fa-house streaming__logo_navlinks_close" : "fa-solid fa-house "}></i>
                  {isSidebarOpen && <p>Home</p>}
                </NavLink>
                <NavLink 
                  to="/user_profile" 
                  className={({isActive}) => (
                  isActive ? "streaming__logo_navlinks streaming__logo_navlinks_active" : "streaming__logo_navlinks" 
                )}
                  state={{ 
                    user: user, 
                    videos: videos
                  }}
                >
                  <i className={isModalOpen ? "fa-solid fa-clipboard-user streaming__logo_navlinks_close" : "fa-solid fa-clipboard-user"}></i>
                  {isSidebarOpen && <p>User profile</p>}
                </NavLink>
                <NavLink to="/settings" className={({isActive}) => (
                  isActive ? "streaming__logo_navlinks streaming__logo_navlinks_active" : "streaming__logo_navlinks" 
                )}>
                  <i className={isModalOpen ? "fa-solid fa-gear streaming__logo_navlinks_close" : "fa-solid fa-gear"}></i>
                  {isSidebarOpen && <p>Settings</p>}
                </NavLink>
              </div>
            </div>
            <div className={isSidebarOpen ? "steaming__sidebar_logout" : "steaming__sidebar_logout streaming__logging_out" }>
              <button><i className={isSidebarOpen ? "fa-solid fa-person-walking-arrow-right" : "fa-solid fa-person-walking-arrow-right fa-flip-horizontal"}></i> {isSidebarOpen && "LOGOUT"}</button>
            </div>
          </div>
      </div>
      {isModalOpen && <UpdateAvatarModal setIsModalOpen={setIsModalOpen} user={user} setUser={setUser}/>}
    </>
  )
}

export default SideBar