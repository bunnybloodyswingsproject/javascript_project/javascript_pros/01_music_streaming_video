import {useState} from 'react';
import useAxiosPrivate from "../hooks/useAxiosPrivate";

const UpdateAvatarModal = ({setIsModalOpen, user, setUser}) => {
    const axiosPrivate = useAxiosPrivate();
    const eyesOption = ["variant26", "variant25", "variant18", "variant17", "variant24", "variant23", "variant22", "variant21", "variant20", "variant19"];
    const mouthOption = ["variant30", "variant29", "variant28", "variant27", "variant26", "variant25", "variant24", "variant23", "variant22", "variant21"];
    const hairOption = ["short16", "short15", "short14", "short13", "short12", "short11", "short10", "short09", "short08", "short07"]
    const hairOptionLong = ["long16", "long15", "long14", "long13", "long12", "long11", "long10", "long09", "long08", "long07"];
    const skinOption = ["variant01", "variant02", "variant03", "variant04", "variant05"]
    const hairColorOption = ["red01", "green", "blue", "pink", "blonde02", "purple", "brown01", "brown02", "black", "gray"];
    const [avatarConfig, setAvatarConfig] = useState({
        "gender": "",
        "eyes": "",
        "mouth": "",
        "hair": "",
        "skin": "",
        "hairColor": ""
    });
    const randomColor = () => {
        return Math.floor(Math.random() * (2-0+1)) + 0;
    }
    const colorOptions = ["FF4A69", "FF8B4E", "FFC44B", "0092F9", "F46500"];
    let AVATAR_DEFAULT_SVG = `https://avatars.dicebear.com/api/initials/${user.firstName}_${user.lastName}.svg?background=%23${colorOptions[randomColor()]}&radius=50`;
    let AVATAR_UPDATING_SVG = `https://avatars.dicebear.com/api/adventurer${avatarConfig.gender === "" ? "" : "/" + avatarConfig.gender}/${user.firstName}_${user.lastName}.svg?background=%23ffffff&radius=50${avatarConfig.eyes === "" ? "" : "&eyes=" + avatarConfig.eyes}${avatarConfig.mouth === "" ? "" : "&mouth=" + avatarConfig.mouth}${avatarConfig.hair === "" ? "" : "&hair=" + avatarConfig.hair}${avatarConfig.skin === "" ? "" : "&skinColor=" + avatarConfig.skin}${avatarConfig.hairColor === "" ? "" : "&hairColor=" + avatarConfig.hairColor}&size=100`;
    const [configProcessCount, setConfigProcessCount] = useState(0);
    const processCounter = [0, 1, 2, 3, 4, 5]
    const [errMsg, setErrMsg] = useState("");

    const handleGenderOption = (e) => {
        if(e.target.value === undefined) {
            setAvatarConfig(prev => {
                return {
                    ...prev,
                    gender: "male"
                }
            })    
        }

        setAvatarConfig(prev => {
            return {
                ...prev,
                gender: e.target.value
            }
        })
    }

    const handleEyeOption = (e) => {
        e.target.classList.add("avatar__toggled_option");
        if(e.target.value === undefined) {
            setAvatarConfig(prev => {
                return {
                    ...prev,
                    eyes: "variant26"
                }
            })    
        }

        setAvatarConfig(prev => {
            // console.log(prev);
            return {
                ...prev,
                eyes: e.target.value
            }
        })
    }

    const handleMouthOption = (e) => {
        setAvatarConfig(prev => {
            return {
                ...prev,
                mouth: "variant30"
            }
        })
    

        setAvatarConfig(prev => {
            // console.log(prev);
            return {
                ...prev,
                mouth: e.target.value
            }
        })
    }

    const handleHairOption = (e) => {
        setAvatarConfig(prev => {
            return {
                ...prev,
                hair: "long04"
            }
        })
    

        setAvatarConfig(prev => {
            // console.log(prev);
            return {
                ...prev,
                hair: e.target.value
            }
        })
    }

    const handleSkinOption = (e) => {
        setAvatarConfig(prev => {
            return {
                ...prev,
                skin: "variant02"
            }
        })
    

        setAvatarConfig(prev => {
            // console.log(prev);
            return {
                ...prev,
                skin: e.target.value
            }
        })
    }

    const handleHairColorOption = (e) => {
        setAvatarConfig(prev => {
            return {
                ...prev,
                hairColor: "red01"
            }
        })
    

        setAvatarConfig(prev => {
            // console.log(prev);
            return {
                ...prev,
                hairColor: e.target.value
            }
        })
    }

    const GenderObject = () => {
        return (
            <>
                <h5>GENDER</h5>
                <div className="streaming__avatar_toggle_gender_buttons">
                    <button className="option_gender01" onClick={handleGenderOption} value="male"><i className="fa-solid fa-mars"></i> Male</button>
                    <button className="option_gender02" onClick={handleGenderOption} value="female"><i className="fa-solid fa-venus"></i> Female</button>
                </div>
            </>
        )
    }

    const EyeObject = () => {
        return (
            <>
                <h5>EYE OPTIONS</h5>
                <div className="streaming__avatar_toggle_buttons">
                    {
                        eyesOption.map((eye, i) => (
                            <button key={i} className={"option" + (i + 1)} onClick={handleEyeOption} value={eyesOption[i]}>{i + 1}</button>        
                        ))
                    }
                </div>
            </>
        )
    }

    const MouthObject = () => {
        return (
            <>
                <h5>MOUTH OPTIONS</h5>
                <div className="streaming__avatar_toggle_buttons">
                    {
                        mouthOption.map((mouth, i) => (
                            <button key={i} className={"option" + (i + 1)} onClick={handleMouthOption} value={mouthOption[i]}>{i + 1}</button>        
                        ))
                    }
                </div>
            </>
        )
    }

    const HairObject = () => {
        return (
            <>
                <h5>HAIR OPTIONS</h5>
                <div className="streaming__avatar_toggle_buttons">
                    {
                        avatarConfig.gender === "male" ?
                        hairOption.map((hair, i) => (
                            <button key={i} className={"option" + (i + 1)} onClick={handleHairOption} value={hairOption[i]}>{i + 1}</button>        
                        ))
                        :
                        hairOptionLong.map((hair, i) => (
                            <button key={i} className={"option" + (i + 1)} onClick={handleHairOption} value={hairOptionLong[i]}>{i + 1}</button>        
                        ))
                    }
                </div>
            </>
        )
    }

    const SkinObject = () => {
        return (
            <>
                <h5>SKIN OPTIONS</h5>
                <div className="streaming__avatar_toggle_skin_buttons">
                    {
                        skinOption.map((skin, i) => (
                            <button key={i} className={"option" + (i + 1)} onClick={handleSkinOption} value={skinOption[i]}>{i + 1}</button>        
                        ))
                    }
                </div>
            </>
        )
    }

    const HairColorObject = () => {
        return (
            <>
                <h5>HAIR COLOR OPTIONS</h5>
                <div className="streaming__avatar_toggle_buttons">
                    {
                        hairColorOption.map((hair, i) => (
                            <button key={i} className={"option" + (i + 1)} onClick={handleHairColorOption} value={hairColorOption[i]}>{i + 1}</button>        
                        ))
                    }
                </div>
            </>
        )
    }

    const handleNextStep = (e) => {
        setConfigProcessCount(prev => prev + 1);
    }

    const handlePreviousStep = (e) => {
        setConfigProcessCount(prev => prev - 1);
    }

    const handleChosenAvatar = async (e) => {
        e.preventDefault();
        try {
            if(AVATAR_UPDATING_SVG === "") return setErrMsg("Avatar configuration issue.");
            const response = await axiosPrivate.post("user/avatarupdate", 
            JSON.stringify({
                _id: user._id,
                image: AVATAR_UPDATING_SVG
            }), {
                headers: { "Content-Type": "application/json" },
                withCredentials: true
            });
    
            if(!response.data.success) {
                setErrMsg(response.data.message);
            }
    
            setUser((prev) => {
                return {
                    ...prev,
                    image: AVATAR_UPDATING_SVG
                }
            });

            window.location.reload(true);
        }catch(error) {
            console.log(error);
        }
    }

  return (
    <div className="streaming__avatar_modifier_container">
        <div className="streaming__avatar_modifer_box">
            <h3>LETS UPDATE YOUR HERO</h3>
            <div className='avatar__straming_error_msg'>
            {errMsg !== "" && <p><i className="fa-solid fa-triangle-exclamation"></i> {errMsg}</p>}
            </div>
            <div className='streaming__avatar_mbox'>
                <div className="streaming__avatar_mbox_formContent">
                    <div className="straming__avatar_mbox_image">
                        <img src={AVATAR_UPDATING_SVG} onError={(e) => {
                            e.target.attributes.src.value = AVATAR_DEFAULT_SVG
                        }} alt="streaming_avatar_iamge" />
                    </div>
                    <div className="streaming__avatar_toggle_options">
                        {
                            configProcessCount === 0 && <GenderObject />
                        }
                        {
                            configProcessCount === 1 && <EyeObject />
                        }
                        {
                            configProcessCount === 2 && <MouthObject />
                        }
                        {
                            configProcessCount === 3 && <HairObject />
                        }
                        {
                            configProcessCount === 4 && <SkinObject />
                        }
                        {
                            configProcessCount === 5 && <HairColorObject />
                        }
                    </div>
                    {
                        configProcessCount === 5 && (
                            <div className="streaming__avatar_mbox_buttons">
                                <button onClick={handleChosenAvatar}>CHOOSE</button>
                            </div>
                        )
                    }
                    {
                        (avatarConfig.gender === "male" || avatarConfig.gender === "female") && (configProcessCount === 0) && (
                            <div className="streaming__avatar_mbox_buttons_next">
                                <button onClick={handleNextStep}>NEXT</button>
                            </div>
                        ) 
                    }
                    {
                        ((configProcessCount === 1) || (configProcessCount === 2) || (configProcessCount === 3) || (configProcessCount === 4)) && (
                            <div className="streaming__avatar_mbox_buttons_option">
                                <button onClick={handlePreviousStep} className="streaming__avatar_mbox_buttons_prevStep">PREVIOUS</button>
                                <button onClick={handleNextStep} className="streaming__avatar_mbox_buttons_nextStep">NEXT</button>
                            </div>
                        )
                    }
                    <div className="streaming__avatar_mbox_progress_radio">
                        {
                            processCounter.map((count, i) => (
                                <div key={i} className={configProcessCount === i ? "active__progress_radio" : ""}></div>
                            ))
                        }
                    </div>
                </div>
            </div>
        </div>
    </div>
  )
}

export default UpdateAvatarModal