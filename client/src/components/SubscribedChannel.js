import React from 'react'
import { useNavigate } from 'react-router-dom';

const SubscribedChannel = ({subscriber, setUser, user, axiosPrivate, setVideos, usersInfo}) => {
    const navigate = useNavigate();
    const onhandleUnsubscribed = async (e) => {
        const subscriber = e.target.attributes.id.value;
        const channelOwner = user;

        const response = await axiosPrivate.post("videos/unsubscribe",
            JSON.stringify({
                subscriber: subscriber,
                channelOwner: channelOwner              
            }),
            {
                headers: { "Content-Type": "application/json" },
                withCredentials: true
            }
       );

       if(!response.data.success) {
            return alert(response.data.message);
       }

       setUser(prev => {
            return {
                ...prev,
                subscriptions: usersInfo?.subscriptions?.filter(subscribed => subscribed._id !== subscriber)
            }
       });
       
       navigate("/");
    }
    return (
    <div className="subscribed_channel_container">
        <div className="subscribed_channel_cardList">
            <div className="subscribed_channel_image_container">
                <img src={subscriber.image} alt="subscribers_img" />
            </div>
            <div className="subscribed_channel_info_container">
                <h3>{subscriber.name}</h3>
                <small>{subscriber.subscribers} {subscriber.subscribers > 1 ? "subscribers" : "subcriber"}</small>
            </div>
            <div className="subscribed_channel_CTA">
                <button id={subscriber._id} onClick={onhandleUnsubscribed}>Subscribed <i className="fa-regular fa-bell"></i></button>
            </div>
        </div>
    </div>
    )
}

export default SubscribedChannel