import React, { useState } from 'react'
import { NavLink } from 'react-router-dom';
const AuthNavbar = () => {
  const [toggleState, setToggleState] = useState(false);
  const toggleHandle = () => {
    setToggleState(prevToggle => !prevToggle);
  }

  return (
    <div className="auth__navbar">
        <div className="auth__brand_logo">
            <div className="auth__logo"></div>
            <h2>BATUGS TV</h2>
        </div>
        <div className="auth__links">
          <div className={toggleState ? "auth_anchor_links openLinks" : "auth_anchor_links closeLinks "}>
            <NavLink to="/register" className={({ isActive }) => 
              (isActive ? "authLnks currentLink" : "authLnks")
          }>Signup</NavLink>
            <NavLink to="/login" className={({ isActive }) => 
              (isActive ? "authLnks currentLink" : "authLnks")
          }>Login</NavLink>
          </div>
          <i className={toggleState ? "fa-solid fa-bars closeToggle" : "fa-solid fa-bars openToggle"} onClick={toggleHandle}></i>
          <i className={toggleState ? "fa-solid fa-xmark openToggle" : "fa-solid fa-xmark closeToggle"} onClick={toggleHandle}></i>
        </div>
    </div>
  )
}

export default AuthNavbar