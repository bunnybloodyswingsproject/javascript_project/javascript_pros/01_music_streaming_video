import React, { useEffect, useRef, useState } from 'react'
import useError from "../hooks/useError";
import storage from "../firebase";
import {ref, uploadBytesResumable, getDownloadURL} from "firebase/storage";
import useAxiosPrivate from '../hooks/useAxiosPrivate';
import { useLocation, useNavigate } from 'react-router-dom';
import useVideo from "../hooks/useVideo";

const UploadForm = ({user, setUser}) => {
    const titleRef = useRef();
    const descriptionRef = useRef();
    const tagsRef = useRef();
    const yearRef = useRef();
    const navigate = useNavigate();
    const location = useLocation();
    const {videos, setVideos} = useVideo();
    const from = location.state?.from?.pathname || "/";
    const [music, setMusic] = useState(null);
    const [uploader, setUploader] = useState(0);
    const {errMsg, setErrMsg} = useError();
    const options = [
        "Rock",
        "Podcast",
        "Mellow Rock",
        "Pop",
        "Country",
        "Grunge",
        "Post Hardcore",
        "Metal",
        "Folk",
        "Hip Hop",
        "Rap",
        "80s",
        "90s",
        "New songs",
        "Ballad",
    ];
    const [video, setVideo] = useState(null);
    const [thumbnail, setThumbnail] = useState(null);
    const [wallpaper, setWallpaper] = useState(null);
    const axiosPrivate = useAxiosPrivate();

    const handleChange = (e) => {
        const value = e.target.value;
        setMusic({...music, [e.target.name]: value});
    }

    const onOptionChangeHandler = (e) => {
        const value = e.target.value;
        setMusic({...music, category: value});
    }

    const onChangeIsFeaturedHandler = (e) => {
        const value = e.target.value === "True" ? true : false;
        setMusic({...music, isfeatured: value});
    }

    const upload = (items) => {
        if(!items[0].file) {
            return setErrMsg("Please upload a video file into the file uploader");
        }

        if(!items[1].file) {
            return setErrMsg("Please upload an image file into the thumnail uploader");
        }

        if(!items[2].file) {
            return setErrMsg("Please upload an image file into the wallpaper uploader");
        }

        if(items[0].label === "video" && items[0].file.type !== "video/mp4") {
            return setErrMsg("File other than mp4 is not allowed in the video uploader.");
        }

        if(items[1].label === "thumbnail" && items[1].file.type !== "image/png") {
            return setErrMsg("File other than png is not allowed in thumbnail uploader.");
        }

        if(items[2].label === "wallpaper" && items[2].file.type !== "image/png") {
            return setErrMsg("File other than png is not allowed in wallpaper uploader.");
        }

        items.forEach(item => {
            const filename = new Date().getTime() + item.label + item.file.name;
            const storageRef = ref(storage, "/batugst_file_uploader/" + filename);
            const uploadTask = uploadBytesResumable(storageRef, item.file);
            uploadTask.on("state_changed", (snapshot) => {
                let progress = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
                console.log(snapshot);
                console.log(`Upload is ${progress}% done`);
                switch(snapshot.state) {
                    case "paused":
                        console.log("Upload is pause");
                        break;
                    case "running":
                        console.log("Upload is running");
                        break;
                    default:
                        console.log("Uploading the files....");
                        break;
                }
            },
            (err) => {console.log(err)},
            () => {
                getDownloadURL(uploadTask.snapshot.ref).then(url => {
                    setMusic(prev => {
                        return {
                            ...prev,
                            [item.label]: url
                        }
                    });

                    setUploader(prev => prev + 1);
                })
            })
        })
    }

    const handleUpload = (e) => {
        if(video === "") {
            return setErrMsg("Please upload a video.");
        }

        e.preventDefault();
        upload([
            {file: video , label: "video"},
            {file: thumbnail, label: "thumbnail"},
            {file: wallpaper, label: "wallpaper"}
        ])
    }

    const onhandleCreate = async (e) => {
        e.preventDefault();
        if(music.title === "") {
            return setErrMsg("Please provide a title");
        }

        if(music.description === "") {
            return setErrMsg("Please provide a description");
        }

        if(music.tags === "") {
            return setErrMsg("Please provide a tag to your uploads");
        }

        if(music.category === "") {
            return setErrMsg("Please provide a category");
        }

        console.log("video", music.video);
        console.log("image", music.thumbnail);
        console.log("wallpaper", music.wallpaper);

        try {
            const response = await axiosPrivate.post("videos/upload", 
                JSON.stringify({
                    userId: user._id,
                    title: music.title,
                    description: music.description,
                    tags: music.tags,
                    category: music.category,
                    video: music.video,
                    thumbnail: music.thumbnail,
                    wallpaper: music.wallpaper,
                    year: music.year,
                    isFeatured: music.isfeatured
                }), {
                    headers: { "Content-type": "application/json" },
                    withCredentials: true
            });

            if(!response.data.success) {
                setErrMsg(response.data.message);
            }

            if(response.data.success) {
                navigate("/", {replace: true});
                window.location.reload(true);
            }

        }catch(error) {
            console.log(error);
        }
    }

    useEffect(() => {
        titleRef.current.focus()
    }, []);

    useEffect(() => {
        setErrMsg("")
    }, [music]);

    return (
    <form className="streaming__settings_uploaderForms">
        <div className="streaming__settings_uploaderContent_thumbnailUploader">
            <div className="streaming_configs streaming__settings_video_uploader">
                <div className="streaming__settings_video_uploader_icon">
                    <i className="fa fa-solid fa-arrow-up fa-5x"></i>
                    <div className="streaming_settings_video_uploader_icon_oblong"></div>
                </div>
                <div className="streaming__settings_video_uploader_description">
                    <h4>Upload</h4>
                    <p>Upload your video here</p>
                </div>
                <input 
                    type="file"
                    name="video"
                    onChange={(e) => setVideo(e.target.files[0])}
                />
            </div>
            <div className="streaming_configs streaming__settings_thumbnail_uploader">
                <div className="streaming__settings_video_uploader_icon_secondary">
                    <i className="fa-regular fa-image fa-5x"></i>
                </div>
                <div className="streaming__settings_video_uploader_description">
                    <h4>Thumbnail</h4>
                    <p>Upload your video thumbnail here</p>
                </div>
                <input 
                    type="file"
                    name="thumbnail"
                    onChange={(e) => setThumbnail(e.target.files[0])}
                />
            </div>
            <div className="streaming_configs streaming__settings_wallpaper_uploader">
                <div className="streaming__settings_video_uploader_icon_secondary">
                <i className="fa-regular fa-images fa-5x"></i>
                </div>
                <div className="streaming__settings_video_uploader_description">
                    <h4>Wallpaper</h4>
                    <p>Upload your video wallpaper here</p>
                </div>
                <input 
                    type="file"
                    name="wallpaper"
                    onChange={(e) => setWallpaper(e.target.files[0])}
                />
            </div>
        </div>
        <div className="streaming__settings_uploaderContent_inputforms">
            <div className="streaming__settings_UCI_left">
                <div className="streaming__settings_UCI_left_form_groups">
                    <label>Music Title</label>
                    <input 
                        type="text"
                        placeholder="Enter Sandman"
                        ref={titleRef}
                        name="title"
                        onChange={handleChange}
                        required={true}
                    />
                </div>
                <div className="streaming__settings_UCI_left_form_groups">
                    <label>Description</label>
                    <textarea 
                        placeholder="I found the love. For me. Baby just dive right in, follow my lead."
                        ref={descriptionRef}
                        name="description"
                        onChange={handleChange}
                        required={true}
                    />
                </div>
                <div className="streaming__settings_UCI_left_form_groups">
                    <label>Tags</label>
                    <input 
                        type="text"
                        placeholder="Ex: Rock, Metal, Punk Rock, Gothic"
                        ref={tagsRef}
                        name="tags"
                        onChange={handleChange}
                        required={true}
                    />
                </div>
            </div>
            <div className="streaming__settings_UCI_right">
                <div className="streaming__settings_UCI_left_form_groups">
                    <label>Category</label>
                    <select onChange={onOptionChangeHandler}>
                        <option>Please choose the music genre</option>
                        {
                            options.map((option, i) => (
                                <option key={i}>{option}</option>
                            ))
                        }
                    </select>
                </div>
                <div className="streaming__settings_UCI_left_form_groups">
                    <label>Do you want this to be your featured video?</label>
                    <select onChange={onChangeIsFeaturedHandler}>
                        <option>False</option>
                        <option>True</option>
                    </select>
                </div>
                <div className="streaming__settings_UCI_left_form_groups">
                    <label>Year</label>
                    <input 
                        placeholder="1986"
                        ref={yearRef}
                        name="year"
                        onChange={handleChange}
                        required={true}
                        type="text"
                    />
                </div>
                <div className="streaming__settings_UCI_left_form_groups">
                    <div className="streaming_settings_UCI_right_form_buttons">
                        <button className="form_buttons_cancel" onClick={() => navigate(-1)}>Cancel</button>
                        {
                            uploader === 3 
                            ? (<button className="form_buttons_create" onClick={onhandleCreate}>Create</button>)
                            : (<button className="form_buttons_create" onClick={handleUpload}>Upload</button>)
                        }
                        <p className="streaming__settings_right_form_buttons_errMsg">{errMsg}</p>
                    </div>
                </div>
            </div>
        </div>
    </form>
    )
}

export default UploadForm