import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { AuthProvider } from './context/AuthProvider';
import { ModalProvider } from './context/ModalProvider';
import { SidebarProvider } from './context/SidebarProvided';
import { ErrorProvider } from './context/ErrorProvider';
import { VideoProvider } from './context/VideoProvider';
import {LayoutProvider} from './context/PageLAyoutModifier';
import { UsersProvider } from './context/UsersProvider';
import { PhoneSidebarProvider } from './context/PhoneNavbarSlider';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <ErrorProvider>
      <AuthProvider>
        <UsersProvider>
          <LayoutProvider>
          <PhoneSidebarProvider>
            <SidebarProvider>
                <ModalProvider>
                  <VideoProvider>
                    <App />
                  </VideoProvider>
                </ModalProvider>
              </SidebarProvider>
            </PhoneSidebarProvider>
          </LayoutProvider>
        </UsersProvider>
      </AuthProvider>
    </ErrorProvider>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
